#include <opxu/opxu.h>
#include <stb/stb-ex.h>
#include <assert.h>
#include <stdio.h>


static float _quad[] = {
		-0.5, -0.5, 0.0,
		+0.5, -0.5, 0.0,
		+0.5, +0.5, 0.0,

		-0.5, -0.5, 0.0,
		+0.5, +0.5, 0.0,
		-0.5, +0.5, 0.0
	};

static void dump_buffer(RTbuffer *buffer)
{
	RTsize w, h, i = -1;
	void *data = NULL;
	RT_ASSERT(rtBufferGetSize2D(*buffer, &w, &h));
	RT_ASSERT(rtBufferMap(*buffer, &data));
	stb_ex_writef("test.bmp", w, h, 4, data);
	RT_ASSERT(rtBufferUnmap(*buffer));
}


int main(int argc, const char *argv[])
{
	struct opxu_context *cp;
	struct opxu_ray_type *rtp;
	struct opxu_buffer2d *b2p;
	struct opxu_buffer1d *b1p;
	struct opxu_geometry *gp;
	struct opxu_material *mp;
	struct opxu_geometry_instance *gip;
	struct opxu_acceleration *ap;
	struct opxu_geometry_group *ggp;

	cp = opxu_context_create(1);
	opxu_context_validate(cp);
	rtp = opxu_context_create_ray_type(cp, 0);
	opxu_ray_type_set_entry_program_with_ptx_file(rtp, "raygen.ptx",
		"raygen");
	opxu_ray_type_set_miss_program_with_ptx_file(rtp, "miss.ptx", "miss");
	b2p = opxu_context_create_buffer2d(cp, "frame_buffer",
		RT_BUFFER_INPUT_OUTPUT, 512, 512, RT_FORMAT_FLOAT4);
	gp = opxu_context_create_geometry(cp, 2);
	opxu_geometry_set_intersection_program_with_ptx_file(gp, "triangle.ptx",
		"triangle_intersect");
	opxu_geometry_set_bounding_box_program_with_ptx_file(gp, "triangle.ptx",
		"triangle_bounding_box");
	b1p = opxu_context_create_buffer1d(cp, "geom_buffer", 
		RT_BUFFER_INPUT_OUTPUT, 6, RT_FORMAT_FLOAT3);
	opxu_buffer1d_set(b1p, _quad, sizeof(_quad));
	mp = opxu_context_create_material(cp);
	opxu_material_set_closest_hit_program_with_ptx_file(mp, 0,
		"material.ptx", "on_closest_hit");
	gip = opxu_context_create_geometry_instance(cp, gp, 1);
	opxu_geometry_instance_set_material(gip, 0, mp);
	ap = opxu_context_create_acceleration(cp, "NoAccel", "NoAccel");
	ggp = opxu_context_create_geometry_group(cp, ap, 1);
	opxu_geometry_group_set_child(ggp, 0, gip);
	opxu_geometry_group_assign_to_variable(ggp, "top_node");
	opxu_context_validate(cp);
	opxu_context_compile(cp);
	opxu_context_run(cp, 0, 512, 512);
	dump_buffer(&b2p->buffer);
	return 0;
}
