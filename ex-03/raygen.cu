#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include "payload.cuh"

using namespace optix;

rtDeclareVariable(uint2, launch_idx, rtLaunchIndex, );
rtBuffer<float4, 2> frame_buffer;

rtDeclareVariable(rtObject, top_node, , );

#define tanfovydiv2 0.577350269 /* = tan(60 Deg / 2) */

/* debug code */
__device__ float4 make_col(float3 v)
{
	if ((v.x > 1.0 || v.x < 0.0) || (v.y > 1.0 || v.y < 0.0)) {
		return make_float4(1.0, 0.0, 0.0, 1.0);
	}

	return make_float4(v.x, v.y, 0.0, 1.0);
}

RT_PROGRAM void raygen()
{
	float w, h;
	float asp;
	float3 eye;
	float3 dir;

	w = (float)frame_buffer.size().x;
	h = (float)frame_buffer.size().y;
	asp = w / h;
	
	/* compute ray direction */
	dir.x = (((float)launch_idx.x + 0.5) / w * 2.0 - 1.0) * asp * 
		tanfovydiv2;
	dir.y = -(((float)launch_idx.y + 0.5) / h * 2.0 - 1.0) * tanfovydiv2; 
	dir.z = 1.0;
	dir = normalize(dir);

	eye = make_float3(0.0, 0.0, -1.0);

	optix::Ray ray = optix::make_Ray(eye, dir, 0, 0.05, RT_DEFAULT_MAX);

	/* init the payload (per-ray data) */
	Payload payload;

	rtTrace(top_node, ray, payload);

	frame_buffer[launch_idx] = payload.color;
	//frame_buffer[launch_idx] = make_float4(0.0, 1.0, 0.0, 1.0);
}
