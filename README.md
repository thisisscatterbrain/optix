NVIDIA OptiX Practice
=====================
*Versions: OptiX 3.0.0, CUDA 5.0*

**ex-00**: simple optix framework; uses a ray generation program to write a output buffer; lines alternate from red to white

**ex-01**: minimal ray tracer; traces a triangle; ray generation, intersection, closest hit and miss programs

**ex-02**: minimal ray tracer; traces a simple geometry (quad); uses a geometry buffer 
