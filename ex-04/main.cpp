#include <optixu/optixpp_namespace.h>
#include <cstdlib>
#include <cstdio>
#include <memory.h>
#include <stb/stb-ex.h>
#include <zeit/timer.h>
#include <obj/file.h>
#include <iu/image.h>
#include <cgm/matrix4.h>
#include <cgm/matrix4-transforms.h>
using namespace optix;

#define WIDTH 512
#define HEIGHT 512

zeit::Timer timer;

static float quad[] = {
		0.0, 0.0, 0.0,
		0.5, 0.0, 0.0,
		0.5, 0.5, 0.0
	};

void setBufferData(Buffer buffer, const void *data, size_t size)
{
	RTsize buf_size;
	buffer->getSize(buf_size);

	printf("buffer element size: %u buffer size : %u, total : %u, input size %u\n",
		buffer->getElementSize(), buf_size, buffer->getElementSize() * buf_size, size);

	void *destination_buffer = buffer->map();
	memcpy(destination_buffer, data, size);
	buffer->unmap();
}

void saveBuffer(Buffer buffer, const char *file_name)
{
	void *buffer_data = buffer->map();
	stb_ex_writef(file_name, WIDTH, HEIGHT, 4, buffer_data);
	iu::Image* image = iu::Image::create((float*)buffer_data, WIDTH, HEIGHT,
		4);
	image->flip();
	image->save("test2.bmp");
	image->destroy();
	buffer->unmap();
}

int main(int argc, const char *argv[])
{
	Context context = Context::create();
	context->setRayTypeCount(1);
	context->setEntryPointCount(1);
	context->setPrintEnabled(true);
	context->setPrintBufferSize(4096);

	Buffer frame_buffer = context->createBuffer(RT_BUFFER_INPUT_OUTPUT,
		RT_FORMAT_FLOAT4, WIDTH, HEIGHT);

	context["frame_buffer"]->set(frame_buffer);

	Program ray_gen_program = context->createProgramFromPTXFile(
		"context.ptx", "raygen");
	context->setRayGenerationProgram(0, ray_gen_program);
	Program miss_program = context->createProgramFromPTXFile("context.ptx",
		"miss");
	context->setMissProgram(0, miss_program);

	/* create geometry and a buffer for holding the actual data */
	obj::File* objfp = obj::File::create("bunny2.obj");
	const obj::Node* nodep = objfp->getNodeWithName("bunny");
	const obj::Array<int>* pos_ids = nodep->getPositionIDs();
	const obj::Array<int>* nrm_ids = nodep->getNormalIDs();
	size_t nfaces = pos_ids->getCount() / 3;

	Geometry geometry = context->createGeometry();
	geometry->setPrimitiveCount(nfaces);
	Program intersection_program = context->createProgramFromPTXFile(
		"geometry.ptx", "intersection");
	geometry->setIntersectionProgram(intersection_program);
	Program bb_program = context->createProgramFromPTXFile(
		"geometry.ptx", "boundingBox");
	geometry->setBoundingBoxProgram(bb_program);

	/* prepare buffers */
	Buffer positions_buffer = context->createBuffer(RT_BUFFER_INPUT_OUTPUT,
		RT_FORMAT_FLOAT3, objfp->getPositions()->getCount() / 3);
	setBufferData(positions_buffer, objfp->getPositions()->getData(),
		objfp->getPositions()->getSize());
	geometry["positions_buffer"]->set(positions_buffer);

	Buffer normals_buffer = context->createBuffer(RT_BUFFER_INPUT_OUTPUT,
		RT_FORMAT_FLOAT3, objfp->getNormals()->getCount() / 3);
	setBufferData(normals_buffer, objfp->getNormals()->getData(),
		objfp->getNormals()->getSize());
	geometry["normals_buffer"]->set(normals_buffer);

	Buffer index_buffer = context->createBuffer(RT_BUFFER_INPUT_OUTPUT,
		RT_FORMAT_INT3, pos_ids->getCount() / 3);
	setBufferData(index_buffer, pos_ids->getData(),
		pos_ids->getSize());
	geometry["index_buffer"]->set(index_buffer);

	Buffer normals_index_buffer = context->createBuffer(RT_BUFFER_INPUT_OUTPUT,
		RT_FORMAT_INT3, nrm_ids->getCount() / 3);
	setBufferData(normals_index_buffer, nrm_ids->getData(),
		nrm_ids->getSize());
	geometry["normals_index_buffer"]->set(normals_index_buffer);

	/* create material for the geometry */
	Material material = context->createMaterial();
	Program material_program = context->createProgramFromPTXFile(
		"material.ptx", "onClosestHit");
	material->setClosestHitProgram(0, material_program);

	GeometryInstance geometry_instance = context->createGeometryInstance();
	geometry_instance->setMaterialCount(1);
	geometry_instance->setGeometry(geometry);
	geometry_instance->setMaterial(0, material);

	Acceleration acceleration = context->createAcceleration("NoAccel",
		"NoAccel");

	GeometryGroup geometry_group = context->createGeometryGroup();
	geometry_group->setChildCount(1);
	geometry_group->setChild(0, geometry_instance);
	geometry_group->setAcceleration(acceleration);

	/* set camera matrix */
	//glm::mat4 w2c = glm::lookAt(glm::vec3(0.0, 0.0, -1.0), 
	//	glm::vec3(0.0,0.0,0), glm::vec3(0.0, 1.0, 0.0));
	//glm::mat4 c2w = glm::inverse(w2c);
	//context["camera_to_world"]->setMatrix4x4fv(true, glm::value_ptr(c2w));
	cgm::Matrix4f c = cgm::makeCameraToWorld(cgm::Vector3f(-0.25, 0.15, -0.1),
		cgm::Vector3f(0.0, 0.0, 0.0), cgm::Vector3f(0.0, 1.0, 0.0));
	context["camera_to_world"]->setMatrix4x4fv(false, c);

	for (int i = 0; i < 16; i++) {
		if (i % 4 == 0) printf("\n");
		printf("%f ", ((float*)c)[i]);
	}

	context["top_node"]->set(geometry_group);
	context->validate();
	puts("tralalal");
	timer.start();
	context->launch(0, WIDTH, HEIGHT);
	timer.stop();
	printf("time elapsed %f\n", timer.getElapsed());
	saveBuffer(frame_buffer, "test.bmp");
	context->destroy();
	return 0;
}
