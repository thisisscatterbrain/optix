#include <optixu/optixu_math_namespace.h>

template<typename T> __inline__ __device__ T interpolateWithTriangle(
	const float3& p0, const float3& p1, const float3& p2, const float3& p, 
	const T& v0, const T& v1, const T& v2)
{
	float3 pp0 = p0 - p;
	float3 pp1 = p1 - p;
	float3 pp2 = p2 - p;
	float a0 = optix::length(optix::cross(pp1, pp2));
	float a1 = optix::length(optix::cross(pp0, pp2));
	float a2 = optix::length(optix::cross(pp0, pp1));
	float a = optix::length(optix::cross(p1 - p0, p2 - p0));
	return (a0 * v0 + a1 * v1 + a2 * v2) / a;
}
