#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include "payload.cuh"

using namespace optix;

#define tanfovydiv2 0.577350269 /* = tan(60 Deg / 2) */

rtDeclareVariable(uint2, launch_index, rtLaunchIndex, );
rtBuffer<float4, 2> frame_buffer;
rtDeclareVariable(rtObject, top_node, , );
rtDeclareVariable(Matrix4x4, camera_to_world, ,);

__device__ inline float3 makeFloat3(float4 v)
{
	float3 rv;
	rv.x = v.x;
	rv.y = v.y;
	rv.z = v.z;
	return rv;
}

RT_PROGRAM void raygen()
{
	float w = (float)frame_buffer.size().x;
	float h = (float)frame_buffer.size().y;
	float asp = w / h;

	/* create a ray */
	float4 ray_origin = make_float4(0.0, 0.0, 0.0, 1.0);
	float4 ray_direction;
	ray_direction.x = (((float)launch_index.x + 0.5) / w * 2.0 - 1.0)
		* asp * tanfovydiv2;
	ray_direction.y = (((float)launch_index.y + 0.5) / w * 2.0 - 1.0)
		* tanfovydiv2;
	ray_direction.z = -1.0;
	ray_direction.w = 1.0;

	//rtPrintf("b: %f %f %f %f\n", ray_direction.x, ray_origin.y, ray_origin.z, ray_origin.w);
	ray_origin = camera_to_world * ray_origin;
	ray_direction = camera_to_world * ray_direction;
	ray_direction = ray_direction - ray_origin;

	Ray ray = make_Ray(makeFloat3(ray_origin), normalize(makeFloat3(ray_direction)), 0, 
		0.05, RT_DEFAULT_MAX);

	/* trace a ray */
	Payload payload;
	rtTrace(top_node, ray, payload);

	/* set the color */
	frame_buffer[launch_index] = payload.color;
}

rtDeclareVariable(Payload, payload, rtPayload, );

RT_PROGRAM void miss()
{
	payload.color = make_float4(0.00, 0.60, 0.90, 1.0);
}

