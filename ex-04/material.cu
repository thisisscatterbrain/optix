#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include "payload.cuh"
using namespace optix;

rtDeclareVariable(Payload, payload, rtPayload,);
rtDeclareVariable(float3, normal, attribute normal, );

RT_PROGRAM void onClosestHit()
{
	rtPrintf("%f %f %f\n", normal.x, normal.y, normal.z);
	float3 light_direction = make_float3(-1.0, 1.0, -1.0);
	light_direction = normalize(light_direction);
	float a = max(dot(light_direction, normal), 0.0);
	payload.color = make_float4(a * 1.0, a * 1.0, a * 1.0, 1.0);
}
