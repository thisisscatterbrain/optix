#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <triint.h>

using namespace optix;

rtDeclareVariable(Ray, ray, rtCurrentRay, );
rtBuffer<float3, 1> positions_buffer;
rtBuffer<int3, 1> index_buffer;
rtBuffer<float3, 1> normals_buffer;
rtBuffer<int3, 1> normals_index_buffer;
rtDeclareVariable(float3, normal, attribute normal, );

RT_PROGRAM void intersection(int index)
{
	int3 idx = index_buffer[index];
	float3 v0 = positions_buffer[idx.x];
	float3 v1 = positions_buffer[idx.y];
	float3 v2 = positions_buffer[idx.z];
	int3 nrm_idx = normals_index_buffer[index];
	float3 n0 = normals_buffer[nrm_idx.x];
	float3 n1 = normals_buffer[nrm_idx.y];
	float3 n2 = normals_buffer[nrm_idx.z];
	float3 n;
	float u, v, t;

	if (intersect_triangle(ray, v0, v1, v2, n, t, u, v)) {
		//normal = normalize(n);
		float3 p = ray.origin + t * ray.direction;
		normal = interpolateWithTriangle<float3>(v0, v1, v2, p, n0, n1, n2);

//		normal = n0;

		if (rtPotentialIntersection(t)) {
			rtReportIntersection(0);
		}
	}
}

RT_PROGRAM void boundingBox(int index, float bb[6])
{
	int3 idx = index_buffer[index];
	float3 v0 = positions_buffer[idx.x];
	float3 v1 = positions_buffer[idx.y];
	float3 v2 = positions_buffer[idx.z];
	bb[0] = min(min(v0.x, v1.x), v2.x);
	bb[1] = min(min(v0.y, v1.y), v2.y);
	bb[2] = min(min(v0.z, v1.z), v2.z);
	bb[3] = max(max(v0.x, v1.x), v2.x);
	bb[4] = max(max(v0.y, v1.y), v2.y);
	bb[5] = max(max(v0.z, v1.z), v2.z);
}
