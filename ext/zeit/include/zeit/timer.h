#ifndef TIMER_H
#define TIMER_H

#include <sys/time.h>

namespace zeit {

class Timer {
public:
	Timer() : started(0.0), stopped(0.0) {}
	virtual ~Timer() {}

	inline void start() {this->started = Timer::getCurrentTime();}
	inline void stop() {this->stopped = Timer::getCurrentTime();}
	inline double getElapsed() const {return this->stopped - this->started;}


private:
	static double getCurrentTime();
	double started;
	double stopped;
};

double Timer::getCurrentTime()
{
 	struct timeval tv;
  	gettimeofday(&tv, 0);
  	return tv.tv_sec+ tv.tv_usec * 1.0e-6;
}


} /* time */

#endif /* end of include guard: TIMER_H */
