#include <stdio.h>
#include "file.h"
#include "array.h"

void dumpVertexData(const obj::Array<float>& vertex_data,
	const char *prefix, int data_dimension)
{
	for (int i = 0; i < vertex_data.getCount(); i += 3) {
		printf("%s", prefix);
	
		for (int j = 0; j < data_dimension; j++) {
			printf("%f ", vertex_data.get(i + j));
		}
		
		printf("\n");
	}
}

void dumpNode(const obj::Node& node)
{
	const obj::Array<const obj::Node *> *children = node.getChildren();
	
	switch (node.getType()) {
	case obj::Node::OBJECT:
		printf("o %s\n", node.getName());
		break;
	case obj::Node::GROUP:
		printf("g %s\n", node.getName());
		break;
	}
	
	for (int i = 0; i < children->getCount(); i++) {
		dumpNode(*children->get(i));
	}
}

void testArray()
{
	obj::Array<int> ap;
	
	for (int i = 0; i < 777; i++) {
		ap.push(i);
	}
	
	for (int i = 0; i < 777; i++) {
		printf("%d\n", ap.get(i));
	}
	
	if (!ap.set(800, 8)) {
		puts("Out of bounds");
	}
	
	ap.reallocate(1000);
	
	if (!ap.set(800, 8)) {
		puts("Out of bounds");
	} else {
		puts("Success");
	}
	
	for (int i = 0; i < 803; i++) {
		printf("%d\n", (ap.get(i)));
	}
	
		
	exit(EXIT_SUCCESS);
}

int main(int argc, const char *argv[])
{
	obj::File *fp = obj::File::create("bunny.obj");

	if (!fp) {
		puts("bunny not found");
		return 0;
	}

	dumpVertexData(*fp->getPositions(), "v ", 3);
	dumpVertexData(*fp->getNormals(), "vn ", 3);
	dumpVertexData(*fp->getUVs(), "vt ", 2);
	dumpNode(*fp->getRoot());
	fp->destroy();
	return 0;
}
