//
//  array.h
//  ObjLoader
//
//  Created by Arno in Wolde Luebke on 03.09.14.
//  Copyright (c) 2014 Arno in Wolde Luebke. All rights reserved.
//
#ifndef ARRAY_H
#define ARRAY_H

#include <cstdlib>
#include <memory>

namespace obj {

template<typename T>
class Array {
public:
	/// Gets the element at index [index].
	/// @param index Index for the element to be queried.
	/// @return The element queried or NULL if [index] is out of bounds.
	inline const T& get(size_t index) const;
	inline T& get(size_t index);
	
	/// Sets an element in the array.
	/// @param index Position of the new element in the array.
	/// @param element Element to be set.
	/// @return true on success, false otherwise.
	inline bool set(size_t index, const T& element);
	
	/// Gets a pointer to the raw data stored by the array. The data is
	/// layed out continuously in memory.
	/// @return Raw data returned.
	inline const T* getData() const {return this->raw_data;}
	inline T* getData() {return this->raw_data;}

	/// Gets the number of elements stored.
	/// @return Number of elements stored.
	inline size_t getCount() const {return this->count;}
	
	/// Gets the size of the raw data in bytes.
	/// @return Size of the raw data in bytes.
	inline size_t getSize() const {return this->count * sizeof(T);}
	
	/// Pushes a new element to the array. Push increases the count of the
	/// array by one.
	/// @param element Element to push.
	/// @return true on success, false otherwise.
	inline bool push(const T& element);
	
	/// Gets the last element of the array.
	/// @return The last element of the array, or NULL if the array is empty.
	inline const T* getBack() const {
		return this->count == 0 ? NULL :
		&this->raw_data[this->count - 1];}
	inline T* getBack() {
		return this->count == 0 ? NULL :
		&this->raw_data[this->count - 1];}

	/// Checks if the array is empty.
	/// @return true if the array is empty. Otherwise false.
	inline bool isEmpty() {return this->count == 0;}

	/// Reallocates the array to [new_count] elements. If [new_count] is
	/// greater than the current count, all new elements are initialized
	/// to zero. If [new_count] is smaller the the current count [push]
	/// does nothing.
	/// @param new_count New # of elements in the array.
	/// @return true on success, false otherwise.
	inline bool reallocate(size_t new_count);

	Array();
	/// Initializes an array that can store [count] elements
	/// @param count The maximum amount of elements the array can store.
	Array(size_t count);
	~Array();
	
private:
	inline bool expand();
	inline bool isOutOfBounds(size_t index) const;

	T* raw_data;
	size_t count;
	size_t max_count;	// internal helper variable, the actual amount
				// [raw_data] could store
};

template<typename T> Array<T>::Array() : raw_data(NULL), count(0), max_count(0)
{

}

template<typename T> Array<T>::Array(size_t count) : count(count),
	max_count(count)
{
	raw_data = (T*)malloc(count * sizeof(T));
}

template<typename T> Array<T>::~Array()
{
	delete raw_data;
}

template<typename T> bool Array<T>::reallocate(size_t new_count)
{
	if (new_count <= this->count) {
		return true;
	} else if (new_count <= this->max_count) {
		this->count = new_count;
		return true;
	} else if (!this->raw_data) {
		this->raw_data = new T[new_count];
		
		if (!this->raw_data) {
			return false;
		}
	} else {
		T* dp = this->raw_data;
		this->raw_data = (T*)realloc(this->raw_data,
			new_count * sizeof(T));
		
		if (!this->raw_data) {
			this->raw_data = dp;
			return false;
		}
		
		// set all new elements to zero
		memset(this->raw_data + this->count, 0,
			(new_count - this->count) * sizeof(T));
		
		this->count = this->max_count = new_count;
	}
	
	return true;
}

#include "array.inl"

}

#endif /* end of include guard: ARRAY_H */
