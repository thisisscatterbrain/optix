#include "file.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

#define MAX_STRLEN 256

//-----------------------------------------------------------------------------
// Node Implementation's definitions
//-----------------------------------------------------------------------------
struct Face {
	int material_index;
	int position_ids[3];
	int normal_ids[3];
	int uv_ids[3];
};
//-----------------------------------------------------------------------------
class obj::Node::NodeImplementation {
public:
	NodeImplementation(const char* name, int type);
	~NodeImplementation();

	bool processLine(const char *line);
	void pushBack(Face& face);
	void pushChild(const Node *node);

	char name[MAX_STRLEN];
	int type;
	Array<int> position_ids;
	Array<int> normal_ids;
	Array<int> uv_ids;
	Array<const Node*> children;
};
//-----------------------------------------------------------------------------
obj::Node::NodeImplementation::NodeImplementation(const char* name,
	int type) : type(type)
{
	strncpy(this->name, name, MAX_STRLEN);
}
//-----------------------------------------------------------------------------
obj::Node::NodeImplementation::~NodeImplementation() {}
//-----------------------------------------------------------------------------
static void updateFace(Face& face)
{
	for (int i = 0; i < 3; i++) {
		face.position_ids[i]--;
		face.normal_ids[i]--;
		face.uv_ids[i]--;
	}
}
//-----------------------------------------------------------------------------
void obj::Node::NodeImplementation::pushChild(const Node *node)
{
	this->children.push(node);
}
//-----------------------------------------------------------------------------
void obj::Node::NodeImplementation::pushBack(Face &face)
{
	updateFace(face);

	for (int i = 0; i < 3; i++) {
		this->position_ids.push(face.position_ids[i]);

		if (face.normal_ids[i] >= 0) {
			this->normal_ids.push(face.normal_ids[i]);
		}

		if (face.uv_ids[i] >= 0) {
			this->uv_ids.push(face.uv_ids[i]);
		}
	}
}
//-----------------------------------------------------------------------------
bool obj::Node::NodeImplementation::processLine(const char *line)
{
	Face f;

	if (strstr(line, "f ") == line) {
		if (3 == sscanf(line, "f %d %d %d", &f.position_ids[0],
			&f.position_ids[1], &f.position_ids[2])) {
			this->pushBack(f);
		} else if (6 == sscanf(line, "f %d/%d %d/%d %d/%d",
			&f.position_ids[0], &f.uv_ids[0], &f.position_ids[1],
			&f.uv_ids[1], &f.position_ids[2], &f.uv_ids[2])) {
			this->pushBack(f);
		} else if (6 == sscanf(line, "f %d//%d %d//%d %d//%d",
			&f.position_ids[0], &f.normal_ids[0],
			&f.position_ids[1], &f.normal_ids[1],
			&f.position_ids[2], &f.normal_ids[2])) {
			this->pushBack(f);
		} else if (9 == sscanf(line, "f %d/%d/%d %d/%d/%d %d/%d/%d",
			&f.position_ids[0], &f.uv_ids[0], &f.normal_ids[0],
			&f.position_ids[1], &f.uv_ids[1], &f.normal_ids[1],
			&f.position_ids[2], &f.uv_ids[2], &f.normal_ids[2])) {
			this->pushBack(f);
		} else {
			return false;
		}
	}

	return true;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Node's initializer
//-----------------------------------------------------------------------------
obj::Node::Node(const char* name, int type)
{
	this->node_implementation = new NodeImplementation(name, type);
}
//-----------------------------------------------------------------------------
obj::Node::~Node()
{
	delete this->node_implementation;
}
//-----------------------------------------------------------------------------
void obj::Node::pushChild(const Node* node)
{
	this->node_implementation->pushChild(node);
}
//-----------------------------------------------------------------------------
int obj::Node::getType() const
{
	return this->node_implementation->type;
}
//-----------------------------------------------------------------------------
const obj::Array<const obj::Node*>* obj::Node::getChildren() const
{
	return &this->node_implementation->children;
}
//-----------------------------------------------------------------------------
const char* obj::Node::getName() const
{
	return this->node_implementation->name;
}
//-----------------------------------------------------------------------------
const obj::Array<int>* obj::Node::getPositionIDs() const
{
	return &this->node_implementation->position_ids;
}
//-----------------------------------------------------------------------------
const obj::Array<int>* obj::Node::getNormalIDs() const
{
	return &this->node_implementation->normal_ids;
}
//-----------------------------------------------------------------------------
const obj::Array<int>* obj::Node::getUVIDs() const
{
	return &this->node_implementation->uv_ids;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// FileImplementation's definitions
//-----------------------------------------------------------------------------
class obj::File::FileImplementation {
public:
	FileImplementation() {};
	~FileImplementation();

	bool initWithFile(FILE* file);
	bool processLine(const char* line);
	Node* createNode(const char* name, int type);
	const Node* getNodeWithName(const char* name) const;

	Array<float> positions;
	Array<float> normals;
	Array<float> uvs;
	Array<Node*> nodes; // nodes managed by this file.

	obj::Node *root;
};
//-----------------------------------------------------------------------------
obj::File::FileImplementation::~FileImplementation()
{
	size_t i = nodes.getCount();
	
	for (i = 0; i < nodes.getCount(); i++) {
		delete nodes.get(i);
	}
}
//-----------------------------------------------------------------------------
bool obj::File::FileImplementation::initWithFile(FILE* file)
{
	this->root = this->createNode("", Node::ROOT);
	
	if (!this->root) {
		return false;
	}

	char line[256];

	while (fgets(line, 256, file)) {
		if(!this->processLine(line)) {
			return false;
		}
	}

	return true;
}
//-----------------------------------------------------------------------------
obj::Node* obj::File::FileImplementation::createNode(const char* name,
	int type)
{
	Node* np = new Node(name, type);

	if (np) {
		this->nodes.push(np);
	}

	return np;
}

//-----------------------------------------------------------------------------
static void pushBack(obj::Array<float>& v_dst, float* v, unsigned int n)
{
	for (unsigned int i = 0; i < n; i++) {
		v_dst.push(v[i]);
	}
}
//-----------------------------------------------------------------------------
static obj::Node* getLastObjectNode(obj::Array<obj::Node*>& nodes)
{
	// Returns the last occurence of an [obj::Node::OBJECT] node in [nodes]
	// or the first element of [nodes].

	int i = (int)nodes.getCount() - 1;
	
	while (i >= 0) {
		if (nodes.get(i)->getType() == obj::Node::OBJECT) {
			return nodes.get(i);
		}
	
		i--;
	}
	
	return nodes.get(0);
}
//-----------------------------------------------------------------------------
bool obj::File::FileImplementation::processLine(const char *line)
{
	float v[3];
	char s[MAX_STRLEN];
	Node* lnp = getLastObjectNode(this->nodes);

	if (strstr(line, "v ") == line) {
		if (3 == sscanf(line, "v %f %f %f", &v[0], &v[1], &v[2])) {
			pushBack(this->positions, v, 3);
		} else {
			return false;
		}
	} else if (strstr(line, "vn ") == line) {
		if (3 == sscanf(line, "vn %f %f %f", &v[0], &v[1], &v[2])) {
			pushBack(this->normals, v, 3);
		} else {
			return false;
		}
	} else if (strstr(line, "vt ") == line) {
		if (3 == sscanf(line, "vt %f %f", &v[0], &v[1])) {
			pushBack(this->uvs, v, 2);
		} else {
			return false;
		}
	} else if (strstr(line, "o ") == line) {
		if (1 == sscanf(line, "o %s", s)) {
			Node* np = this->createNode(s,
				Node::OBJECT);
			
			if (!np) {
				return false;
			}
			
			this->root->pushChild(np);
		} else {
			return false;
		}
	} else if (strstr(line, "g ") == line) {
		if (1 == sscanf(line, "g %s", s)) {
			if (lnp->getType() == Node::ROOT ||
				lnp->getType() == Node::GROUP) {
				return false;
			}
		
			Node* np = this->createNode(s, Node::GROUP);
			
			if (!np) {
				return false;
			}
			
			lnp->pushChild(np);
		} else {
			return false;
		}
	} else {
		if (!lnp->node_implementation->processLine(line)) {
			return false;
		}
	}

	return true;
}
//-----------------------------------------------------------------------------
const obj::Node* obj::File::FileImplementation::getNodeWithName(
	const char* name) const
{
	for (size_t i = 0; i < this->nodes.getCount(); i++) {
		Node* np = this->nodes.get(i);

		if (!strncmp(np->getName(), name, MAX_STRLEN)) {
			return np;
		}
	}

	return NULL;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// File creation functions
//-----------------------------------------------------------------------------
obj::File::File()
{
	this->file_implementation = new FileImplementation();
}
//-----------------------------------------------------------------------------
obj::File::~File()
{
	delete this->file_implementation;
}
//-----------------------------------------------------------------------------
obj::File* obj::File::create(const char* file_name)
{
	File* op = new File();

	if (!op) {
		return NULL;
	}

	FILE *fp = fopen(file_name, "r");

	if (!fp) {
		goto error;
	}

	if (!op->file_implementation->initWithFile(fp))
	{
		goto error;
	}

	fclose(fp);
	return op;
error:
	fclose(fp);
	op->destroy();
	return NULL;
}
//-----------------------------------------------------------------------------
obj::Node *obj::File::createNode(const char* name, int type)
{
	return this->file_implementation->createNode(name, type);
}
//-----------------------------------------------------------------------------
void obj::File::destroy()
{
	delete this;
}
//-----------------------------------------------------------------------------
const obj::Array<float>* obj::File::getPositions() const
{
	return &this->file_implementation->positions;
}
//-----------------------------------------------------------------------------
const obj::Array<float>* obj::File::getNormals() const
{
	return &this->file_implementation->normals;
}
//-----------------------------------------------------------------------------
const obj::Array<float>* obj::File::getUVs() const
{
	return &this->file_implementation->uvs;
}
//-----------------------------------------------------------------------------
const obj::Node* obj::File::getRoot() const
{
	return this->file_implementation->root;
}
//-----------------------------------------------------------------------------
const obj::Node* obj::File::getNodeWithName(const char* name) const
{
	return this->file_implementation->getNodeWithName(name);
}
//-----------------------------------------------------------------------------
