#ifndef FILE_H
#define FILE_H

#include "array.h"

namespace obj {

class Node {
friend class File;
public:

	/// The types a node can take.
	enum {
		ROOT = 0,
		OBJECT,
		GROUP
	};

	/// Gets the name of the node
	/// @return The name of the node.
	const char* getName() const;
	
	/// Adds a child to the node.
	/// @param The child to be added.
	void pushChild(const Node* node);

	/// Gets the type of the node
	/// @return The type of the node.
	int getType() const;

	/// Gets the children of the node
	/// @return An array of the children of the node.
	const Array<const obj::Node*>* getChildren() const;
	
	const Array<int>* getPositionIDs() const;
	const Array<int>* getNormalIDs() const;
	const Array<int>* getUVIDs() const;
			
private:
	Node();
	Node(const Node& original);
	Node& operator=(const Node& original);
	Node(const char* name, int type);
	~Node();
	class NodeImplementation;
	NodeImplementation *node_implementation;
};

class File {
public:
	/// Loads an .obj file from disk.
	/// @param file_name The .obj file's file name.
	/// @return The a pointer to a [File] class, or NULL if something went
	///	wrong.
	static File* create(const char *file_name);
	
	/// Creates a managed .obj node. The node is managed by the [File] class.
	/// @param name Name of the node.
	/// @param type Type of the node.
	/// @return The created node or NULL if something went wrong.
	Node* createNode(const char* name, int type);
	
	/// Destroys the file and all its resources
	void destroy();
	
	/// Gets the array of floats that stores all positions in the .obj file.
	/// @return The array of positions.
	const Array<float>* getPositions() const;

	/// Gets an array of floats that stores all normals in the .obj file.
	/// @return The array of normals.
	const Array<float>* getNormals() const;

	/// Gets an array of floats that stores all uvs in the .obj file.
	/// @return The array of uvs.
	const Array<float>* getUVs() const;
	
	/// Gets root node of an .obj file's scene graph.
	/// @return The root node of the .obj file's scene graph.
	const Node* getRoot() const;
private:
	File();
	virtual ~File();
	File(const File& original);
	File& operator=(const File& original);

	class FileImplementation;
	FileImplementation* file_implementation;
};

}
#endif /* end of include guard: FILE_H */
