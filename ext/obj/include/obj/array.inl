const int GROW = 2;
//-----------------------------------------------------------------------------
template<typename T> bool Array<T>::expand()
{
	if (this->count < this->max_count) {
		return true;
	}
	
	T* dp = this->raw_data;
	size_t new_count = 1;
	
	if (this->count != 0) {
		new_count = GROW * this->max_count;
	}
	
	this->raw_data = (T*)realloc(this->raw_data,
		new_count * sizeof(T));
	
	if (!this->raw_data) {
		this->raw_data = dp;
		return false;
	}
		
	this->max_count = new_count;
	return true;
}
//-----------------------------------------------------------------------------
template<typename T> const T& Array<T>::get(size_t index) const
{
	if (this->isOutOfBounds(index)) {
		//return NULL;
	}
	
	return this->raw_data[index];
}
//-----------------------------------------------------------------------------
template<typename T> T& Array<T>::get(size_t index)
{
	if (this->isOutOfBounds(index)) {
		//return NULL;
	}
	
	return this->raw_data[index];
}
//-----------------------------------------------------------------------------
template<typename T> bool Array<T>::set(size_t index, const T& element)
{
	if (this->isOutOfBounds(index)) {
		return false;
	}
	
	this->raw_data[index] = element;
	return true;
}
//-----------------------------------------------------------------------------
template<typename T> bool Array<T>::isOutOfBounds(size_t index) const
{
	return index >= this->count;
}
//-----------------------------------------------------------------------------
template<typename T> bool Array<T>::push(const T& element)
{
	if(!this->expand()) {
		return false;
	}
	
	this->raw_data[this->count] = element;
	this->count++;
	return true;
}
//-----------------------------------------------------------------------------
