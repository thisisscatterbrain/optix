//
//  main.cpp
//  test
//
//  Created by Arno in Wolde Luebke on 05.09.14.
//  Copyright (c) 2014 Arno in Wolde Luebke. All rights reserved.
//

#include "tuple.h"
#include "vector3.h"
#include "vector4.h"
#include "matrix4.h"
#include "matrix4_transforms.h"
#include <stdio.h>

int main()
{
	cgm::Tuple<3, float> v;
	cgm::Vector4<float> vec;

	cgm::Matrix4f m = cgm::makeCameraToWorld<float>(cgm::Vector3f(0, 0, 0),
		cgm::Vector3f(0.0, 1.0, 0.0), cgm::Vector3f(0.0, 0.0, 1.0));
	cgm::Matrix4f m2;
	m2.makeCameraToWorld(cgm::Vector3f(0, 0, 0),
		cgm::Vector3f(0.0, 1.0, 0.0), cgm::Vector3f(0.0, 0.0, 1.0));

	if (m == m2) {
		puts("is equal");
	} else {
		puts("not equal");
	}

	v[0] = 1;
	printf("%f\n", v[0]);
	return 0;
}