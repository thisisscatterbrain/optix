#ifndef TUPLE_H__
#define TUPLE_H__

#include <string>
#include <sstream>
#include <iostream>

namespace cgm {

template<unsigned int DIM, typename TYPE>
class Tuple
{
public:
    Tuple();
    ~Tuple() {};
    Tuple(const Tuple& orig);
    Tuple& operator=(const Tuple& orig);
    inline Tuple<DIM, TYPE>& operator=(const TYPE& a);
    inline operator const TYPE*() const {return data;}
    inline operator TYPE*() {return data;}
    inline const TYPE& operator[](unsigned int i) const;
    inline TYPE& operator[](unsigned int i);

protected:
    TYPE data[DIM];
};

#include "Tuple.inl"

typedef Tuple<2, unsigned int> Tuple2ui;
typedef Tuple<3, unsigned int> Tuple3ui;

}

#endif /* end of include guard: TUPLE_H__ */