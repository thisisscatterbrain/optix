//-----------------------------------------------------------------------------
template<typename T>
Matrix4<T>::Matrix4(
	const Vector4<T>& row0,
	const Vector4<T>& row1,
	const Vector4<T>& row2,
	const Vector4<T>& row3
)
{
	data[0] = row0;
	data[1] = row1;
	data[2] = row2;
	data[3] = row3;
}
//------------------------------------------------------------------------------
template<typename T>
Vector4<T>& Matrix4<T>::operator[](unsigned int i)
{
	return data[i];
}
//------------------------------------------------------------------------------
template<typename T>
const Vector4<T>& Matrix4<T>::operator[](unsigned int i) const
{
	return data[i];
}
//-----------------------------------------------------------------------------
template<typename T>
Vector3<T> Matrix4<T>::operator*(const Vector3<T>& v) const
{
	T x = data[0][0]*v[0] + data[0][1]*v[1] + data[0][2]*v[2] + data[0][3];
	T y = data[1][0]*v[0] + data[1][1]*v[1] + data[1][2]*v[2] + data[1][3];
	T z = data[2][0]*v[0] + data[2][1]*v[1] + data[2][2]*v[2] + data[2][3];
	T w = data[3][0]*v[0] + data[3][1]*v[1] + data[3][2]*v[2] + data[3][3];

	return Vector3<T>(x/w, y/w, z/w);
}
//-----------------------------------------------------------------------------
template<typename T>
Vector4<T> Matrix4<T>::operator*(const Vector4<T>& v) const
{
	T x = data[0][0]*v[0] + data[0][1]*v[1] + data[0][2]*v[2] + data[0][3]*v[3];
	T y = data[1][0]*v[0] + data[1][1]*v[1] + data[1][2]*v[2] + data[1][3]*v[3];
	T z = data[2][0]*v[0] + data[2][1]*v[1] + data[2][2]*v[2] + data[2][3]*v[3];
	T w = data[3][0]*v[0] + data[3][1]*v[1] + data[3][2]*v[2] + data[3][3]*v[3];

	return Vector4<T>(x, y, z, w);
}
//-----------------------------------------------------------------------------
template<typename T>
Matrix4<T> Matrix4<T>::operator*(const Matrix4<T>& m) const
{
	Matrix4<T> res;

	for (int i = 0; i < 4; i++) 
	{
		for (int j = 0; j < 4; j++) 
		{
			for (int k = 0; k < 4; k++) 
			{
				res[i][j] += data[i][k] * m[k][j];
			}
		}
	}

	return res;
}
//-----------------------------------------------------------------------------
template<typename T>
void Matrix4<T>::transpose()
{
	unsigned int k = 1;
	for (unsigned int i = 0; i < 4; i++)
	{
		for (unsigned int j = k; j < 4; j++)
		{
			T ele = (*this)[i][j];
			(*this)[i][j] = (*this)[j][i];
			(*this)[j][i] = ele;
		}
		k++;
	}
}
//------------------------------------------------------------------------------
template<typename T>
inline void Matrix4<T>::makeScale(const T& sx, const T& sy, const T& sz)
{
	this->makeZero();
	data[0][0] = sx;
	data[1][1] = sy;
	data[2][2] = sz;
	data[3][3] = T(1);
}
//-----------------------------------------------------------------------------
template<typename T>
inline void Matrix4<T>::makeTranslation(const T& x, const T& y, const T& z)
{
	this->makeIdentity();
	data[0][3] = x;
	data[1][3] = y;
	data[2][3] = z;
}
//-----------------------------------------------------------------------------
template<typename T>
void Matrix4<T>::makePerspective(
	const T& fovy, 
	const T& aspect, 
	const T& near, 
	const T& far
)
{
	T t = tanf(fovy/2.0);
	T h = near*t;
	T w = h*aspect;

	this->makePerspective(-w, w, -h, h, near, far);
}
//------------------------------------------------------------------------------
template<typename T>
void Matrix4<T>::makePerspective(
	const T& l, const T& r, 
	const T& b, const T& t, 
	const T& n, const T& f
)
{
	T* raw = static_cast<T*>(data[0]);

	raw[0] = 2.0f*n/(r - l);
	raw[1] = 0.0f;
	raw[2] = 0.0f;
	raw[3] = 0.0f;

	raw[4] = 0.0f;
	raw[5] = 2.0f*n/(t - b);
	raw[6] = 0.0f;
	raw[7] = 0.0f;

	raw[8] = (r + l)/(r - l);
	raw[9] = (t + b)/(t - b);
	raw[10] = -(f + n)/(f - n);
	raw[11] = -1.0f;

	raw[12] = 0.0f;
	raw[13] = 0.0f;
	raw[14] = -2.0f*f*n/(f - n);
	raw[15] = 0.0f;	 

	this->transpose();
}
//------------------------------------------------------------------------------
template<typename T>
void Matrix4<T>::makeWorldToCamera(
	const Vector3<T>& eye, 
	const Vector3<T>& f,
	const Vector3<T>& up
)
{
	Vector3<T> n = eye - f;
	n.Normalize();

	Vector3<T> u = up.cross(n);
	u.Normalize();

	Vector3<T> v = n.cross(u);

	T* raw = static_cast<T*>(data[0]);

	raw[0] = u[0];
	raw[4] = u[1];
	raw[8] = u[2];
	raw[12] = -u.Dot(eye);

	raw[1] = v[0];
	raw[5] = v[1];
	raw[9] = v[2];
	raw[13] = -v.Dot(eye);   

	raw[2] = n[0];
	raw[6] = n[1];
	raw[10] = n[2];
	raw[14] = -n.Dot(eye);   

	raw[3] = 0.0f;
	raw[7] = 0.0f;
	raw[11] = 0.0f;
	raw[15] = 1.0f;

	this->transpose();
}
//------------------------------------------------------------------------------
template<typename T>
void Matrix4<T>::makeCameraToWorld(
	const Vector3<T>& eye, 
	const Vector3<T>& f,
	const Vector3<T>& up
)
{
	Vector3<T> n = eye - f;
	n.normalize();

	Vector3<T> u = up.cross(n);
	u.normalize();

	Vector3<T> v = n.cross(u);

	T* raw = static_cast<T*>(data[0]);

	raw[0] = u[0];
	raw[4] = u[1];
	raw[8] = u[2];
	raw[12] = 0;

	raw[1] = v[0];
	raw[5] = v[1];
	raw[9] = v[2];
	raw[13] = 0;   

	raw[2] = n[0];
	raw[6] = n[1];
	raw[10] = n[2];
	raw[14] = 0;   

	raw[3] = eye[0];
	raw[7] = eye[1];
	raw[11] = eye[2];
	raw[15] = 1.0f;
}
//------------------------------------------------------------------------------
template<typename T>
void Matrix4<T>::makeIdentity()
{
	for (unsigned int i = 0; i < 4; i++)
	{
		for (unsigned int j = 0; j < 4; j++)
		{
			if (i == j)
			{
				data[i][j] = static_cast<T>(1);
			}
			else
			{
				data[i][j] = static_cast<T>(0);
			}
		}
	}
}
//------------------------------------------------------------------------------
template<typename T>
void Matrix4<T>::makeZero()
{
	for (unsigned int i = 0; i < 4; i++)
	{
		for (unsigned int j = 0; j < 4; j++)
		{
			data[i][j] = static_cast<T>(0);
		}
	}
}
//------------------------------------------------------------------------------
template<typename T>
void Matrix4<T>::makeRotationX(const T& angle)
{
	this->MakeZero();

	(*this)[0][0] = static_cast<T>(1);
	(*this)[1][1] = std::cos(angle);
	(*this)[1][2] = -std::sin(angle);
	(*this)[2][1] = std::sin(angle);
	(*this)[2][2] = std::cos(angle);
	(*this)[3][3] = static_cast<T>(1);
}
//------------------------------------------------------------------------------
template<typename T>
void Matrix4<T>::makeRotationY(const T& angle)
{
	this->MakeZero();

	(*this)[0][0] = std::cos(angle);
	(*this)[0][2] = std::sin(angle);
	(*this)[1][1] = static_cast<T>(1);
	(*this)[2][0] = -std::sin(angle);
	(*this)[2][2] = std::cos(angle);	
	(*this)[3][3] = static_cast<T>(1);
}
//------------------------------------------------------------------------------
template<typename T>
void Matrix4<T>::makeRotationZ(const T& angle)
{
	this->MakeZero();

	(*this)[0][0] = std::cos(angle);
	(*this)[0][1] = -std::sin(angle);
	(*this)[1][0] = std::sin(angle);
	(*this)[1][1] = std::cos(angle);
	(*this)[2][2] = static_cast<T>(1);
	(*this)[3][3] = static_cast<T>(1);
}
//------------------------------------------------------------------------------
template<typename T>
inline T Matrix4<T>::computeDeterminant() const
{
	#define a(i,j) data[i][j]
	float det;
	
	det = a(0,0)*(a(1,1)*a(2,2)*a(3,3)+a(1,2)*a(2,3)*a(3,1)+a(1,3)*a(2,1)*a(3,2)
				 -a(1,1)*a(2,3)*a(3,2)-a(1,2)*a(2,1)*a(3,3)-a(1,3)*a(2,2)*a(3,1))
		+ a(0,1)*(a(1,0)*a(2,3)*a(3,2)+a(1,2)*a(2,0)*a(3,3)+a(1,3)*a(2,2)*a(3,0)
				 -a(1,0)*a(2,2)*a(3,3)-a(1,2)*a(2,3)*a(3,0)-a(1,3)*a(2,0)*a(3,2))
		+ a(0,2)*(a(1,0)*a(2,1)*a(3,3)+a(1,1)*a(2,3)*a(3,0)+a(1,3)*a(2,0)*a(3,1)
				 -a(1,0)*a(2,3)*a(3,1)-a(1,1)*a(2,0)*a(3,3)-a(1,3)*a(2,1)*a(3,0))
		+ a(0,3)*(a(1,0)*a(2,2)*a(3,1)+a(1,1)*a(2,0)*a(3,2)+a(1,2)*a(2,1)*a(3,0)
				 -a(1,0)*a(2,1)*a(3,2)-a(1,1)*a(2,2)*a(3,0)-a(1,2)*a(2,0)*a(3,1));
	
	#undef a // end of a
	return det;
}
//------------------------------------------------------------------------------
template<typename T>
bool Matrix4<T>::invert()
{
	this->Transpose();
	T inv[16], det;
	T* m = &data[0][0];

	inv[0] = m[5]  * m[10] * m[15] - 
			 m[5]  * m[11] * m[14] - 
			 m[9]  * m[6]  * m[15] + 
			 m[9]  * m[7]  * m[14] +
			 m[13] * m[6]  * m[11] - 
			 m[13] * m[7]  * m[10];

	inv[4] = -m[4]  * m[10] * m[15] + 
			  m[4]  * m[11] * m[14] + 
			  m[8]  * m[6]  * m[15] - 
			  m[8]  * m[7]  * m[14] - 
			  m[12] * m[6]  * m[11] + 
			  m[12] * m[7]  * m[10];

	inv[8] = m[4]  * m[9] * m[15] - 
			 m[4]  * m[11] * m[13] - 
			 m[8]  * m[5] * m[15] + 
			 m[8]  * m[7] * m[13] + 
			 m[12] * m[5] * m[11] - 
			 m[12] * m[7] * m[9];

	inv[12] = -m[4]  * m[9] * m[14] + 
			   m[4]  * m[10] * m[13] +
			   m[8]  * m[5] * m[14] - 
			   m[8]  * m[6] * m[13] - 
			   m[12] * m[5] * m[10] + 
			   m[12] * m[6] * m[9];

	inv[1] = -m[1]  * m[10] * m[15] + 
			  m[1]  * m[11] * m[14] + 
			  m[9]  * m[2] * m[15] - 
			  m[9]  * m[3] * m[14] - 
			  m[13] * m[2] * m[11] + 
			  m[13] * m[3] * m[10];

	inv[5] = m[0]  * m[10] * m[15] - 
			 m[0]  * m[11] * m[14] - 
			 m[8]  * m[2] * m[15] + 
			 m[8]  * m[3] * m[14] + 
			 m[12] * m[2] * m[11] - 
			 m[12] * m[3] * m[10];

	inv[9] = -m[0]  * m[9] * m[15] + 
			  m[0]  * m[11] * m[13] + 
			  m[8]  * m[1] * m[15] - 
			  m[8]  * m[3] * m[13] - 
			  m[12] * m[1] * m[11] + 
			  m[12] * m[3] * m[9];

	inv[13] = m[0]  * m[9] * m[14] - 
			  m[0]  * m[10] * m[13] - 
			  m[8]  * m[1] * m[14] + 
			  m[8]  * m[2] * m[13] + 
			  m[12] * m[1] * m[10] - 
			  m[12] * m[2] * m[9];

	inv[2] = m[1]  * m[6] * m[15] - 
			 m[1]  * m[7] * m[14] - 
			 m[5]  * m[2] * m[15] + 
			 m[5]  * m[3] * m[14] + 
			 m[13] * m[2] * m[7] - 
			 m[13] * m[3] * m[6];

	inv[6] = -m[0]  * m[6] * m[15] + 
			  m[0]  * m[7] * m[14] + 
			  m[4]  * m[2] * m[15] - 
			  m[4]  * m[3] * m[14] - 
			  m[12] * m[2] * m[7] + 
			  m[12] * m[3] * m[6];

	inv[10] = m[0]  * m[5] * m[15] - 
			  m[0]  * m[7] * m[13] - 
			  m[4]  * m[1] * m[15] + 
			  m[4]  * m[3] * m[13] + 
			  m[12] * m[1] * m[7] - 
			  m[12] * m[3] * m[5];

	inv[14] = -m[0]  * m[5] * m[14] + 
			   m[0]  * m[6] * m[13] + 
			   m[4]  * m[1] * m[14] - 
			   m[4]  * m[2] * m[13] - 
			   m[12] * m[1] * m[6] + 
			   m[12] * m[2] * m[5];

	inv[3] = -m[1] * m[6] * m[11] + 
			  m[1] * m[7] * m[10] + 
			  m[5] * m[2] * m[11] - 
			  m[5] * m[3] * m[10] - 
			  m[9] * m[2] * m[7] + 
			  m[9] * m[3] * m[6];

	inv[7] = m[0] * m[6] * m[11] - 
			 m[0] * m[7] * m[10] - 
			 m[4] * m[2] * m[11] + 
			 m[4] * m[3] * m[10] + 
			 m[8] * m[2] * m[7] - 
			 m[8] * m[3] * m[6];

	inv[11] = -m[0] * m[5] * m[11] + 
			   m[0] * m[7] * m[9] + 
			   m[4] * m[1] * m[11] - 
			   m[4] * m[3] * m[9] - 
			   m[8] * m[1] * m[7] + 
			   m[8] * m[3] * m[5];

	inv[15] = m[0] * m[5] * m[10] - 
			  m[0] * m[6] * m[9] - 
			  m[4] * m[1] * m[10] + 
			  m[4] * m[2] * m[9] + 
			  m[8] * m[1] * m[6] - 
			  m[8] * m[2] * m[5];

	det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

	if (det == 0)
	{
		this->transpose();
		return false;
	}

	det = 1.0 / det;

	for (int i = 0; i < 16; i++)
	{
		m[i] = inv[i] * det;
	}

	this->transpose();

	return true;
}
//-----------------------------------------------------------------------------
template<typename T> bool cgm::Matrix4<T>::operator==(const Matrix4<T> &rhs)
{
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			if (this->data[i][j] != rhs[i][j]) {
				return false;
			}
		}
	}
	
	return true;
}
//------------------------------------------------------------------------------
