#ifndef VECTOR3_H
#define VECTOR3_H

#include <cmath>
#include "Tuple.h"

namespace cgm {

template<typename T>
class Vector3 : public Tuple<3, T> {
public:
	Vector3() {}
	Vector3(const T& x, const T& y, const T& z);
	~Vector3() {};

	inline operator const T*() const {return data;}
	inline operator T*() {return data;}
	inline const T& operator[](unsigned int i) const;
	inline T& operator[](unsigned int i);
	inline const T& getX() const {return data[0];}
	inline const T& getY() const {return data[1];}
	inline const T& getZ() const {return data[2];}
	inline Vector3<T> operator-(const Vector3<T>& v) const;
	inline Vector3<T> operator+(const Vector3<T>& v) const;
	inline Vector3<T> operator*(const T& a) const;
	inline Vector3<T>& operator+=(const Vector3<T>& v);
	inline Vector3<T> cross(const Vector3<T>& v) const;
	inline T dot(const Vector3<T>& v) const;
	inline bool normalize();
	inline T getMagnitude() const;
	
private:
	using Tuple<3, T>::data;
};

typedef Vector3<float> Vector3f;
typedef Vector3<int> Vector3i;
typedef Vector3<unsigned int> Vector3ui;

#include "Vector3.inl" 
}
#endif /* end of include guard: VECTOR3_H__ */
