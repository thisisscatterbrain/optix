#ifndef VECTOR4_H
#define VECTOR4_H

#include "Tuple.h"

namespace cgm {

template<typename T>
class Vector4 : public Tuple<4, T> {
public:
	Vector4() {}
	Vector4(const T& x, const T& y, const T& z);
	Vector4(const T& x, const T& y, const T& z, const T& w);
	~Vector4() {};
	inline operator const T*() const {return data;}
	inline operator T*() {return data;}
	inline Vector4<T>& operator=(const T& a);
	inline const T& operator[](unsigned int i) const;
	inline T& operator[](unsigned int i);
	inline const T& getX() const {return data[0];}
	inline const T& getY() const {return data[1];}
	inline const T& getZ() const {return data[2];}
	inline const T& getW() const {return data[3];}

private:
	using Tuple<4, T>::data;
};
  
#include "Vector4.inl" 
	 
typedef Vector4<float> Vector4f;
typedef Vector4<int> Vector4i;
typedef Vector4<unsigned int> Vector4ui;

}

#endif /* end of include guard: VECTOR3_H__ */
