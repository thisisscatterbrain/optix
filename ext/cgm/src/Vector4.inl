//------------------------------------------------------------------------------
template<typename T>
Vector4<T>::Vector4(const T& x, const T& y, const T& z)
{
	data[0] = x;
	data[1] = y;
	data[2] = z;
}
//------------------------------------------------------------------------------
template<typename T>
Vector4<T>::Vector4(const T& x, const T& y, const T& z, const T& w)
{
	data[0] = x;
	data[1] = y;
	data[2] = z;
	data[3] = w;
}
//------------------------------------------------------------------------------
template<typename T>
Vector4<T>& Vector4<T>::operator=(const T& a)
{
	data = a;
	return *this;
}
//------------------------------------------------------------------------------
template<typename T>
T& Vector4<T>::operator[](unsigned int i)
{
	return data[i];
}
//------------------------------------------------------------------------------
template<typename T>
const T& Vector4<T>::operator[](unsigned int i) const
{
	return data[i];
}
//------------------------------------------------------------------------------
