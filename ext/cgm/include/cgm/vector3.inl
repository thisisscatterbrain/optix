//------------------------------------------------------------------------------
template<typename T>
Vector3<T>::Vector3(const T& x, const T& y, const T& z)
{
	data[0] = x;
	data[1] = y;
	data[2] = z;
}
//------------------------------------------------------------------------------
template<typename T>
T& Vector3<T>::operator[](unsigned int i)
{

	return data[i];
}
//------------------------------------------------------------------------------
template<typename T>
const T& Vector3<T>::operator[](unsigned int i) const
{
	
	return data[i];
}
//------------------------------------------------------------------------------
template<typename T>
bool Vector3<T>::normalize()
{
	T norm = sqrt(data[0]*data[0] + data[1]*data[1] + data[2]*data[2]);

	if (norm == 0.0)
	{
		return false;
	}

	data[0] /= norm;
	data[1] /= norm;
	data[2] /= norm;

	return true;
}
//------------------------------------------------------------------------------
template<typename T>
Vector3<T> Vector3<T>::operator-(const Vector3<T>& v) const
{
	Vector3<T> result;

	result[0] = (*this)[0] - v[0];
	result[1] = (*this)[1] - v[1];
	result[2] = (*this)[2] - v[2];

	return result;
}
//------------------------------------------------------------------------------
template<typename T>
Vector3<T> Vector3<T>::operator+(const Vector3<T>& v) const
{
	Vector3<T> result;

	result[0] = (*this)[0] + v[0];
	result[1] = (*this)[1] + v[1];
	result[2] = (*this)[2] + v[2];

	return result;
}
//------------------------------------------------------------------------------
template<typename T>
Vector3<T>& Vector3<T>::operator+=(const Vector3<T>& v)
{
	(*this)[0] += v[0];
	(*this)[1] += v[1];
	(*this)[2] += v[2];

	return *this;
}
//------------------------------------------------------------------------------
template<typename T>
Vector3<T> Vector3<T>::cross(const Vector3<T>& v) const
{
	Vector3<T> result;

	result[0] = (*this)[1]*v[2] - (*this)[2]*v[1];
	result[1] = -((*this)[0]*v[2] - (*this)[2]*v[0]);
	result[2] = (*this)[0]*v[1] - (*this)[1]*v[0]; 
	
	return result;
}
//------------------------------------------------------------------------------
template<typename T>
T Vector3<T>::dot(const Vector3<T>& v) const
{
	return (*this)[0]*v[0] + (*this)[1]*v[1] + (*this)[2]*v[2];
}
//------------------------------------------------------------------------------
template<typename T>
Vector3<T> Vector3<T>::operator*(const T& a) const
{
	Vector3<T> result;
	
	result[0] = (*this)[0]*a;
	result[1] = (*this)[1]*a;
	result[2] = (*this)[2]*a;

	return result;
} 
//------------------------------------------------------------------------------
template<typename T>
T Vector3<T>::getMagnitude() const
{
	return sqrt( 
		this->data[0]*this->data[0] + 
		this->data[1]*this->data[1] + 
		this->data[2]*this->data[2]
	);
}
//-----------------------------------------------------------------------------

