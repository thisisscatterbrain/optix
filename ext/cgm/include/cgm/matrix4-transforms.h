#ifndef MATRIX4_H__
#define MATRIX4_H__
 
#include "matrix4.h"

namespace cgm {

template <typename T> Matrix4<T> makeCameraToWorld(
	const Vector3<T>& eye,
	const Vector3<T>& f,
	const Vector3<T>& up)
{
	Vector3<T> n = eye - f;
	n.normalize();
	Vector3<T> u = up.cross(n);
	u.normalize();
	Vector3<T> v = n.cross(u);
	Matrix4<T> rv;
	T* raw = static_cast<T*>(rv);
	raw[0] = u[0];
	raw[4] = u[1];
	raw[8] = u[2];
	raw[12] = 0.0; 
	raw[1] = v[0];
	raw[5] = v[1];
	raw[9] = v[2];
	raw[13] = 0.0; 
	raw[2] = n[0];
	raw[6] = n[1];
	raw[10] = n[2];
	raw[14] = 0.0; 
	raw[3] = eye[0];
	raw[7] = eye[1];
	raw[11] = eye[2];
	raw[15] = 1.0f;
	return rv;
}

};

#endif /* end of include guard: MATRIX4_H__ */
