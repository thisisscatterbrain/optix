//------------------------------------------------------------------------------
template<unsigned int DIM, typename TYPE>
Tuple<DIM, TYPE>::Tuple()
{
    memset(this->data, 0, sizeof(TYPE)*DIM);
}
//------------------------------------------------------------------------------
template<unsigned int DIM, typename TYPE>
Tuple<DIM, TYPE>::Tuple(const Tuple& orig)
{
    *this = orig; 
}
//------------------------------------------------------------------------------
template<unsigned int DIM, typename TYPE>
Tuple<DIM, TYPE>& Tuple<DIM, TYPE>::operator=(const Tuple& orig)
{
    for (unsigned int i = 0; i < DIM; i++)
    {
        data[i] = orig.data[i];
    }
    return *this;    
}
//------------------------------------------------------------------------------
template<unsigned int DIM, typename TYPE>
Tuple<DIM, TYPE>& Tuple<DIM, TYPE>::operator=(const TYPE& a)
{
    for (unsigned int i = 0; i < DIM; i++)
    {
        data[i] = a;
    }
    return *this;    
}
//------------------------------------------------------------------------------
template<unsigned int DIM, typename TYPE>
TYPE& Tuple<DIM, TYPE>::operator[](unsigned int i)
{
    return data[i];
}
//------------------------------------------------------------------------------
template<unsigned int DIM, typename TYPE>
const TYPE& Tuple<DIM, TYPE>::operator[](unsigned int i) const
{
    return data[i];
}
//------------------------------------------------------------------------------