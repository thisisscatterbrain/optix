#ifndef MATRIX4_H
#define MATRIX4_H

#include "vector4.h"
#include "vector3.h"

namespace cgm {

/// Declaration of a 4x4 Matrix. Note memory is layed out in ROW MAJOR 
/// fashion.
template<typename T>
class Matrix4
{
public:
	Matrix4() {};
	Matrix4(const Vector4<T>& row0, const Vector4<T>& row1,
		const Vector4<T>& row2, const Vector4<T>& row3);
	~Matrix4() {};

	inline const Vector4<T>& operator[](unsigned int i) const;
	inline Vector4<T>& operator[](unsigned int i);
	inline Matrix4<T> operator*(const Matrix4<T>& m) const;
	inline Vector3<T> operator*(const Vector3<T>& v) const;
	inline Vector4<T> operator*(const Vector4<T>& v) const;

	/// Gets the raw data in form of a linear array with 16 elements.
	operator const T*() const {return &data[0][0];}
	operator T*() {return &data[0][0];}
	
	inline bool operator==(const Matrix4<T>& rhs);
	
	
	inline void makePerspective(const T& l, const T& r,
		const T& b, const T& t, const T& n, const T& f);
	inline void makePerspective(const T& fovy, const T& aspect,
		const T& near, const T& far);
	inline void makeWorldToCamera(const Vector3<T>& eye,
		const Vector3<T>& f, const Vector3<T>& up);
	inline void makeCameraToWorld(const Vector3<T>& eye,
		const Vector3<T>& f, const Vector3<T>& up);
	inline void makeRotationX(const T& angle);
	inline void makeRotationY(const T& angle);
	inline void makeRotationZ(const T& angle);
	inline void makeScale(const T& sx, const T& sy, const T& sz);
	inline void makeTranslation(const T& x, const T& y, const T& z);
	inline void makeZero();
	inline void makeIdentity();
	inline T computeDeterminant() const;
	inline bool invert();
	inline void transpose();	

private:	
	Vector4<Vector4<T> > data;
};

#include "matrix4.inl"

typedef Matrix4<float> Matrix4f;

}

#endif /* end of include guard: MATRIX4_H */
