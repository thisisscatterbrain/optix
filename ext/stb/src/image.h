#ifndef IMAGE_H
#define IMAGE_H

namespace stb {

template<typename DIM>
class Image {
public:
	Image(const char* file_name);
	Image(float* raw_data, unsigned int width, unsigned int height);
	Image(unsigned char* raw_data, unsigned int width, unsigned int height);
	Image(unsigned int witdh, unsigned int height);
	virtual ~Image();
	int save(const char* file_name) const;
	unsigned int getWidth() const {return this->width;}
	unsigned int getHeight() const {return this->height;}
	unsigned int getNumChannels() const {return this->num_channels;}
	operator unsigned char*() {return raw_data;}
	void flip(bool horizontal = false);
	void destroy();

private:
	unsigned int width;
	unsigned int height;
	unsigned int num_channels;
	unsigned char* raw_data;
};
	
} /* stb */

#endif /* end of include guard: IMAGE_H */
