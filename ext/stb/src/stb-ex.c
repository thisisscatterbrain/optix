#include "stb-ex.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>


int stb_ex_writef(char const *filename, int w, int h, int n, const void *data)
{
	const float *fdp = (const float *)data;
	unsigned char *cdp = malloc(w * h * n * sizeof(unsigned char));
	int rv = 1;
	int i = 0;

	if (!cdp) {
		return 0;	
	}
	
	for (i = 0; i < w * h * n; i++) {
		cdp[i] = (unsigned char)(fmaxf(0.0, fminf(fdp[i], 1.0f)) * 255.0);
	}

	if (strstr(filename, ".bmp")) {
		rv = stbi_write_bmp(filename, w, h, n, cdp);	
	} else {
		rv = 0;
	}
	
	free(cdp);
	return rv;
}
