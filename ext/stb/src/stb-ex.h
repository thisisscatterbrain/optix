#ifndef STB_EX_H
#define STB_EX_H

#ifdef __cplusplus
extern "C"
{
#endif

int stb_ext_write(char const *filename, int w, int h, int n, const void *data);
int stb_ext_writef(char const *filename, int w, int h, int n, const void *data);

#ifdef __cplusplus
}
#endif

#endif /* end of include guard: STB-EX_H */
