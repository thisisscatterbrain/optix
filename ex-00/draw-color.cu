#include <optix.h>

rtDeclareVariable(uint2, launch_index, rtLaunchIndex, );
rtBuffer<float4, 2> buffer;

RT_PROGRAM void draw()
{
	if (launch_index.y & 2) {
		buffer[launch_index].x = 1.0; 	
		buffer[launch_index].y = 0.0; 	
		buffer[launch_index].z = 0.0; 	
		buffer[launch_index].w = 1.0;
	} else {
		buffer[launch_index].x = 1.0; 	
		buffer[launch_index].y = 1.0; 	
		buffer[launch_index].z = 1.0; 	
		buffer[launch_index].w = 1.0;
	}
}
