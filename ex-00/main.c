#include <stdio.h>
#include <optix.h>
#include <assert.h>
#include <stb/stb-ex.h>

#define RT_ASSERT(X) assert(X == RT_SUCCESS);

#define PTX_FILENAME "draw-color.ptx"
#define FUNCTIONNAME "draw"
#define WIDTH 512
#define HEIGHT 512

static void dump_buffer(RTbuffer *buffer)
{
	RTsize w, h, i = -1;
	void *data = NULL;
	RT_ASSERT(rtBufferGetSize2D(*buffer, &w, &h));
	RT_ASSERT(rtBufferMap(*buffer, &data));
	stb_ex_writef("test.bmp", w, h, 4, data);
	RT_ASSERT(rtBufferUnmap(*buffer));
}

int main(int argc, const char *argv[])
{
	RTcontext context;
	RTbuffer buffer;
	RTprogram program;
	RTvariable bufvar;

	RT_ASSERT(rtContextCreate(&context));
	RT_ASSERT(rtContextSetRayTypeCount(context, 1));
	RT_ASSERT(rtContextSetEntryPointCount(context, 1));
	RT_ASSERT(rtBufferCreate(context, RT_BUFFER_INPUT_OUTPUT, & buffer));
	RT_ASSERT(rtBufferSetFormat(buffer, RT_FORMAT_FLOAT4));
	RT_ASSERT(rtBufferSetSize2D(buffer, WIDTH, HEIGHT));
	//RT_ASSERT(rtBufferSetElementSize(buffer, sizeof(float) * 4));
	RT_ASSERT(rtContextDeclareVariable(context, "buffer", &bufvar));
	RT_ASSERT(rtVariableSetObject(bufvar, buffer));

	/* create a program */
	RT_ASSERT(rtProgramCreateFromPTXFile(context, PTX_FILENAME,
		FUNCTIONNAME, &program));
	RT_ASSERT(rtContextSetRayGenerationProgram(context, 0, program));

	/* compile the context & run it */
	RT_ASSERT(rtContextCompile(context));
	RT_ASSERT(rtContextLaunch2D(context, 0, WIDTH, HEIGHT));

	/* check results */
	dump_buffer(&buffer);

	/* clean up */
	RT_ASSERT(rtProgramDestroy(program));
	RT_ASSERT(rtBufferDestroy(buffer));
	RT_ASSERT(rtContextDestroy(context));
	return 0;
}

