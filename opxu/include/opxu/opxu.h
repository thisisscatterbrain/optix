#ifndef OPXU_H
#define OPXU_H

#include <optix.h>
#include <stdlib.h>
#include "common.h"

#ifdef __cplusplus
extern "C"
{
#endif

/* forward declarations */
struct opxu_geometry;
struct opxu_ray_type;
struct opxu_buffer1d;
struct opxu_buffer2d;
struct opxu_material;
struct opxu_geometry_instance;
struct opxu_acceleration;
struct opxu_geometry_group;

/******************************************************************************
 *
 * 			Context
 *
 *****************************************************************************/

struct opxu_context {
	RTcontext context;
	unsigned int num_ray_types;
	unsigned int num_entry_points;

	struct opxu_ray_type *ray_types;
	struct opxu_geometry *geometries;
	struct opxu_buffer1d *buffers1d;
	struct opxu_buffer2d *buffers2d;
	struct opxu_material *materials;
	struct opxu_geometry_instance *geometry_instances;
	struct opxu_acceleration *accelerations;
	struct opxu_geometry_group *geometry_groups;
};

struct opxu_context *opxu_context_create(unsigned int num_ray_types);
void opxu_context_destroy(struct opxu_context **context);
void opxu_context_validate(struct opxu_context *context);
void opxu_context_compile(struct opxu_context *context);
void opxu_context_run(struct opxu_context *context,
	unsigned int entry_point_index, unsigned int width,
	unsigned int height);

/******************************************************************************
 *
 * 			Ray type
 *
 *****************************************************************************/

/**
 * A ray type is identified by its index [idx]. A ray type is defined by
 * a ray generation program (a entry point program) that specifies how the ray
 * is created and a miss program that handles the case when a ray misses
 * the geometry. The ray type structure is managed by the context.
 */
struct opxu_ray_type {
	unsigned int id;
	RTcontext context; /* the context managing the ray */
	RTprogram entry_program;
	RTprogram miss_program;
	struct opxu_ray_type *next;
};

/**
 * Creates a ray type and adds it to the context.
 * @param id The identifier for the ray.
 * @return NULL in case of failure, a pointer to the created ray type struct
 *         otherwise.
 */
struct opxu_ray_type *opxu_context_create_ray_type(struct opxu_context *context,
	unsigned int id);


/**
 * Sets the entry point for a ray type. The entry point is defined as a function
 * in a .ptx file. The function reports an [OPXU_BAD_PROGRAM] error if creating
 * the program fails.
 * @param type Ray type the entry point should be set for.
 * @param file_name Reference to the .ptx file holding the entry program.
 * @param function_name Name of the function in the .ptx file specifying the
 * 		entry point.
 */
void opxu_ray_type_set_entry_program_with_ptx_file(struct opxu_ray_type *type,
	const char *file_name, const char *function_name);


/**
 * Sets the miss program for a ray type. The entry point is defined as a
 * function in a .ptx file. The function reports an [OPXU_BAD_PROGRAM] error
 * if creating the program fails.
 * @param type Ray type the entry point should be set for.
 * @param file_name Reference to the .ptx file holding the miss program.
 * @param function_name Name of the function in the .ptx file specifying the
 * 		the miss function.
 */
void opxu_ray_type_set_miss_program_with_ptx_file(struct opxu_ray_type *type,
	const char *file_name, const char *function_name);

/******************************************************************************
 *
 * 			Geometry
 *
 *****************************************************************************/

struct opxu_geometry {
	RTcontext context;
	RTgeometry geometry;
	unsigned int num_primitives;
	RTprogram intersection_program;
	RTprogram bounding_box_program;
	struct opxu_geometry *next;
};

/**
 * Creates geometry that holds [num_primitives] primitives and adds it to the
 * context. The function reports [OPXU_ERROR_OUT_OF_MEMORY] on failure and
 * returns NULL.
 * @param context Context the geometry is added to.
 * @param num_primitives Number of primitives managed by the geometry.
 * @return NULL on failure, the created geometry struct otherwise.
 */
struct opxu_geometry *opxu_context_create_geometry(struct opxu_context *context,
	unsigned int num_primitives);

/**
 * Sets the intersection program for [geometry]. The program is assumed to be
 * specified by a function [function_name] in a .ptx file ([file_name]). The
 * function reports [OPXU_ERROR_BAD_PROGRAM] in case of failure.
 * @param geometry The geometry we want to to set the intersection program for.
 * @param file_name File name of the .ptx file that contains the intersection
 * 	program.
 * @param function_name Function name of the intersection program in the .ptx
 * 	file.
 */
void opxu_geometry_set_intersection_program_with_ptx_file(
	struct opxu_geometry *geometry, const char *file_name,
	const char *function_name);

/**
 * Sets the bounding box program for [geometry]. The program is assumed to be
 * specified by a function [function_name] in a .ptx file ([file_name]). The
 * function reports [OPXU_ERROR_BAD_PROGRAM] in case of failure.
 * @param geometry The geometry we want to to set the intersection program for.
 * @param file_name File name of the .ptx file that contains the bounding box
 * 	program.
 * @param function_name Function name of the bounding box program in the .ptx
 * 	file.
 */
void opxu_geometry_set_bounding_box_program_with_ptx_file(
	struct opxu_geometry *geometry, const char *file_name,
	const char *function_name);

/******************************************************************************
 *
 * 			Buffer 1D
 *
 *****************************************************************************/

struct opxu_buffer1d {
	char name[OPXU_MAX_STRLEN]; /** variable name of the buffer */
	unsigned int type; 	    /** type of the buffer */
	unsigned int num_elements;
	int element_format;
	RTvariable variable; 	     /* the variable the buffer is bound to */
	RTbuffer buffer;
	RTcontext context;
	struct opxu_buffer1d *next;
};

struct opxu_buffer1d *opxu_context_create_buffer1d(struct opxu_context *context,
	const char *name, unsigned int type, unsigned int num_elements,
	int element_format);

void opxu_buffer1d_set(struct opxu_buffer1d *buffer, void *data, size_t size);

/******************************************************************************
 *
 * 			Buffer 2D
 *
 *****************************************************************************/

struct opxu_buffer2d {
	char name[OPXU_MAX_STRLEN]; /** variable name of the buffer */
	unsigned int type; 	    /** type of the buffer */
	unsigned int width, height;
	int element_format;
	RTvariable variable; 	     /* the variable the buffer is bound to */
	RTbuffer buffer;
	RTcontext context;
	struct opxu_buffer2d *next;
};

struct opxu_buffer2d *opxu_context_create_buffer2d(struct opxu_context *context,
	const char *name, unsigned int type, unsigned int width,
	unsigned int height, int element_format);

struct opxu_buffer2d *opxu_context_get_buffer2d(struct opxu_context *context,
	const char *name);

/******************************************************************************
 *
 * 			Material
 *
 *****************************************************************************/

struct opxu_material {
	RTcontext context;
	RTmaterial material;
	struct opxu_material *next;
};

struct opxu_material *opxu_context_create_material(
	struct opxu_context *context);

void opxu_material_set_closest_hit_program_with_ptx_file(
	struct opxu_material *material, unsigned int ray_type_index,
	const char *file_name, const char *function_name);


/******************************************************************************
 *
 * 			geometry instance
 *
 *****************************************************************************/

struct opxu_geometry_instance {
	RTcontext context;
	RTgeometryinstance geometry_instance;
	struct opxu_geometry_instance *next;
};

struct opxu_geometry_instance *opxu_context_create_geometry_instance(
	struct opxu_context *context, struct opxu_geometry *geometry,
	unsigned int num_materials);

void opxu_geometry_instance_set_material(
	struct opxu_geometry_instance *geometry_instance,
	unsigned int material_index, struct opxu_material *material);

/******************************************************************************
 *
 * 			Acceleration
 *
 *****************************************************************************/

struct opxu_acceleration {
	RTcontext context;
	RTacceleration acceleration;
	struct opxu_acceleration *next;
};

struct opxu_acceleration *opxu_context_create_acceleration(
	struct opxu_context *context, const char *builder,
	const char *traverser);


/******************************************************************************
 *
 * 			Geometry Group
 *
 *****************************************************************************/

struct opxu_geometry_group {
	RTcontext context;
	RTgeometrygroup geometry_group;
	RTvariable variable;
	struct opxu_geometry_group *next;
};

struct opxu_geometry_group *opxu_context_create_geometry_group(
	struct opxu_context *context, struct opxu_acceleration *acceleration,
	unsigned int num_children);

void opxu_geometry_group_set_child(struct opxu_geometry_group *geometry_group,
	unsigned int index, struct opxu_geometry_instance *geometry_instance);

void opxu_geometry_group_assign_to_variable(
	struct opxu_geometry_group *geometry_group, const char *name);

#ifdef __cplusplus
}
#endif

#endif /* end of include guard: OPXU_H */
