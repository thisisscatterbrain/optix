#ifndef COMMON_H
#define COMMON_H

#include <optix.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define OPXU_MAX_STRLEN 64

/*
 * Potential errors.
 */
enum {
	OPXU_NO_ERROR = 0,
	OPXU_ERROR_OUT_OF_MEMORY,
	OPXU_ERROR_INVALID_ARGUMENTS,
	OPXU_ERROR_BAD_PROGRAM
};

/* error code */
#define opxu_error_set(optix_error, opxu_error)  			\
	opxu_error_set2(optix_error, opxu_error, __FILE__, 		\
		__FUNCTION__, __LINE__); 				\

void opxu_error_set2(int optix_error, int opxu_error, const char *file_name,
	const char *function_name, int line);
int opxu_error_get();

/*
 * Assert for optix functions.
 */
#define RT_ASSERT(X) assert(X == RT_SUCCESS);


#ifdef __cplusplus
}
#endif

#endif /* end of include guard: COMMON_H */
