#include "common.h"
#include <stdio.h>

static char *_error_strings[] = {
		"No error",
		"Out of memory",
		"Invalid arguments",
		"Bad program"
	};

static int _opxu_error = OPXU_NO_ERROR;

void opxu_error_set2(int optix_error, int opxu_error, const char *file_name,
	const char *function_name, int line)
{
	const char* s;	

	rtContextGetErrorString(NULL, optix_error, &s);
	printf("In file \"%s\", function \"%s\" line %d: %s\n", file_name,
		function_name, line, _error_strings[opxu_error]);
	
	if (optix_error != RT_SUCCESS) {
		printf("\toptix: %s\n", s);
	}

	_opxu_error = opxu_error;
}

int opxu_error_get()
{
	int rv = _opxu_error;
	_opxu_error = OPXU_NO_ERROR;
	return rv;
}
