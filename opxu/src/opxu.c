#include "opxu.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

#define H_ERR(optix_error_code, opxu_error_code) 			\
	if (optix_error_code != RT_SUCCESS) { 				\
		opxu_error_set(optix_error_code, opxu_error_code);	\
		goto error; 						\
	} 								\

/******************************************************************************
 *
 * 				Context
 *
 *****************************************************************************/

struct opxu_context *opxu_context_create(unsigned int num_ray_types)
{
	struct opxu_context *context = calloc(1, sizeof(*context));
	int err;

	if (!context) {
		return NULL;
	}

	if (RT_SUCCESS != rtContextCreate(&context->context)) {
		goto error;
	}

	err = rtContextSetRayTypeCount(context->context, num_ray_types);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	err = rtContextSetEntryPointCount(context->context, num_ray_types);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	return context;
error:
	opxu_context_destroy(&context);
	return NULL;
}

void opxu_context_destroy(struct opxu_context **context)
{
}

void opxu_context_run(struct opxu_context *context,
	unsigned int entry_point_index, unsigned int width,
	unsigned int height)
{
	int err;
	assert(context);
	err = rtContextLaunch2D(context->context, entry_point_index, width,
		height);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
error:
	return;
}

void opxu_context_validate(struct opxu_context *context)
{
	int err;
	assert(context);
	err = rtContextValidate(context->context);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
error:
	return;
}

void opxu_context_compile(struct opxu_context *context)
{
	int err;
	assert(context);
	err = rtContextCompile(context->context);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
error:
	return;
}

/******************************************************************************
 *
 * 				Ray type
 *
 *****************************************************************************/

/*
 * creates a raytype and adds it to the context
 */
static struct opxu_ray_type *context_create_ray_type(
	struct opxu_context *context, unsigned int id)
{
	struct opxu_ray_type *rt = calloc(1, sizeof(*rt));

	if (!rt) {
		opxu_error_set(OPXU_ERROR_OUT_OF_MEMORY, RT_SUCCESS);
		return NULL;
	}

	rt->context = context->context;
	rt->id = id;
	rt->next = context->ray_types;
	context->ray_types = rt;
	return rt;
}

struct opxu_ray_type *opxu_context_create_ray_type(struct opxu_context *context,
	unsigned int id)
{
	return context_create_ray_type(context, id);
}


void opxu_ray_type_set_entry_program_with_ptx_file(struct opxu_ray_type *type,
	const char *file_name, const char *function_name)
{
	int err;

	assert(type);

	err = rtProgramCreateFromPTXFile(type->context,
		file_name, function_name, &type->entry_program);
	H_ERR(err, OPXU_ERROR_BAD_PROGRAM);

	err = rtContextSetRayGenerationProgram(type->context,
		type->id, type->entry_program);
	H_ERR(err, OPXU_ERROR_BAD_PROGRAM);
error:
	return;
}

void opxu_ray_type_set_miss_program_with_ptx_file(struct opxu_ray_type *type,
	const char *file_name, const char *function_name)
{
	int err;

	assert(type);

	err = rtProgramCreateFromPTXFile(type->context,
		file_name, function_name, &type->miss_program);
	H_ERR(err, OPXU_ERROR_BAD_PROGRAM);

	err = rtContextSetMissProgram(type->context,
		type->id, type->miss_program);
	H_ERR(err, OPXU_ERROR_BAD_PROGRAM);
error:
	return;
}

/******************************************************************************
 *
 * 				Geometry
 *
 *****************************************************************************/

static struct opxu_geometry *context_create_geometry(
	struct opxu_context *context, unsigned int num_primitives)
{
	struct opxu_geometry *gp = calloc(1, sizeof(*gp));
	int err;

	if (!gp) {
		opxu_error_set(OPXU_ERROR_OUT_OF_MEMORY, OPXU_NO_ERROR);
		return NULL;
	}

	gp->num_primitives = num_primitives;
	err = rtGeometryCreate(context->context, &gp->geometry);
	H_ERR(err, OPXU_ERROR_OUT_OF_MEMORY);
	err = rtGeometrySetPrimitiveCount(gp->geometry, num_primitives);
	H_ERR(err, OPXU_ERROR_OUT_OF_MEMORY);
	gp->context = context->context;
	gp->next = context->geometries;
	context->geometries = gp;
	return gp;
error:
	free(gp);
	return NULL;
}

struct opxu_geometry *opxu_context_create_geometry(struct opxu_context *context,
	unsigned int num_primitives)
{
	return context_create_geometry(context, num_primitives);
}

void opxu_geometry_set_intersection_program_with_ptx_file(
	struct opxu_geometry *geometry, const char *file_name,
	const char *function_name)
{
	int err;

	assert(geometry);

	err = rtProgramCreateFromPTXFile(geometry->context,
		file_name, function_name, &geometry->intersection_program);
	H_ERR(err, OPXU_ERROR_BAD_PROGRAM);
	err = rtGeometrySetIntersectionProgram(geometry->geometry,
		geometry->intersection_program);
	H_ERR(err, OPXU_ERROR_BAD_PROGRAM);
error:
	return;
}

void opxu_geometry_set_bounding_box_program_with_ptx_file(
	struct opxu_geometry *geometry, const char *file_name,
	const char *function_name)
{
	int err;

	assert(geometry);

	err = rtProgramCreateFromPTXFile(geometry->context,
		file_name, function_name, &geometry->bounding_box_program);
	H_ERR(err, OPXU_ERROR_BAD_PROGRAM);
	err = rtGeometrySetBoundingBoxProgram(geometry->geometry,
		geometry->bounding_box_program);
	H_ERR(err, OPXU_ERROR_BAD_PROGRAM);
error:
	return;
}

/******************************************************************************
 *
 * 				Buffer 1D
 *
 *****************************************************************************/

static struct opxu_buffer1d *context_create_buffer1d(
	struct opxu_context *context, const char *name, unsigned int type,
	unsigned int num_elements, int element_format)
{
	struct opxu_buffer1d *bp = calloc(1, sizeof(*bp));
	int err;

	if (!bp) {
		opxu_error_set(OPXU_ERROR_OUT_OF_MEMORY, OPXU_NO_ERROR);
		goto error;
	}

	err = rtBufferCreate(context->context, type, &bp->buffer);
	H_ERR(err, OPXU_ERROR_OUT_OF_MEMORY);
	err = rtBufferSetFormat(bp->buffer, element_format);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	err = rtBufferSetSize1D(bp->buffer, num_elements);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	err = rtContextDeclareVariable(context->context, name, &bp->variable);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	err = rtVariableSetObject(bp->variable, bp->buffer);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	strncpy(bp->name, name, OPXU_MAX_STRLEN);
	bp->type = type;
	bp->num_elements = num_elements;
	bp->element_format = element_format;
	bp->context = context->context;
	bp->next = context->buffers1d;
	context->buffers1d = bp->next;
	return bp;
error:
	free(bp);
	return NULL;
}

struct opxu_buffer1d *opxu_context_create_buffer1d(struct opxu_context *context,
	const char *name, unsigned int type, unsigned int num_elements,
	int element_format)
{
	return context_create_buffer1d(context, name, type, num_elements,
		element_format);
}

void opxu_buffer1d_set(struct opxu_buffer1d *buffer, void *data, size_t size)
{
	void *dp;
	int err;

	assert(buffer);

	err = rtBufferMap(buffer->buffer, &dp);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	memcpy(dp, data, size);
	rtBufferUnmap(buffer->buffer);
error:
	return;
}

/******************************************************************************
 *
 * 				Buffer 2D
 *
 *****************************************************************************/

static struct opxu_buffer2d *context_create_buffer2d(
	struct opxu_context *context, const char *name, unsigned int type,
	unsigned int width, unsigned int height, int element_format)
{
	struct opxu_buffer2d *bp = calloc(1, sizeof(*bp));
	int err;

	if (!bp) {
		opxu_error_set(OPXU_ERROR_OUT_OF_MEMORY, RT_SUCCESS);
		goto error;
	}

	err = rtBufferCreate(context->context, type, &bp->buffer);
	H_ERR(err, OPXU_ERROR_OUT_OF_MEMORY);
	err = rtBufferSetFormat(bp->buffer, element_format);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	err = rtBufferSetSize2D(bp->buffer, width, height);
	H_ERR(err, OPXU_ERROR_OUT_OF_MEMORY);
	err = rtContextDeclareVariable(context->context, name, &bp->variable);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	err = rtVariableSetObject(bp->variable, bp->buffer);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	strncpy(bp->name, name, OPXU_MAX_STRLEN);
	bp->type = type;
	bp->width = width;
	bp->height = height;
	bp->element_format = element_format;
	bp->context = context->context;
	bp->next = context->buffers2d;
	context->buffers2d = bp->next;
	return bp;
error:
	free(bp);
	return NULL;
}

struct opxu_buffer2d *opxu_context_create_buffer2d(struct opxu_context *context,
	const char *name, unsigned int type, unsigned int width,
	unsigned int height, int element_format)
{
	return context_create_buffer2d(context, name, type, width, height,
		element_format);
}

struct opxu_buffer2d *opxu_context_get_buffer2d(struct opxu_context *context,
	const char *name)
{
	struct opxu_buffer2d *bp = context->buffers2d;
	assert(context);

	while (bp) {

		if (!strncpy(bp->name, name, OPXU_MAX_STRLEN)) {
			return bp;
		}

		bp = bp->next;
	}

	return NULL;
}

/******************************************************************************
 *
 * 			Material
 *
 *****************************************************************************/

struct opxu_material *opxu_context_create_material(struct opxu_context *context)
{
	struct opxu_material *mp = calloc(1, sizeof(*mp));
	int err;

	if (!mp) {
		opxu_error_set(OPXU_ERROR_OUT_OF_MEMORY, RT_SUCCESS);
		return NULL;
	}

	assert(context);

	err = rtMaterialCreate(context->context, &mp->material);
	H_ERR(err, OPXU_ERROR_OUT_OF_MEMORY);
	mp->context = context->context;
	mp->next = context->materials;
	context->materials = mp;
	return mp;
error:
	free(mp);
	return NULL;
}

void opxu_material_set_closest_hit_program_with_ptx_file(
	struct opxu_material *material, unsigned int ray_type_index,
	const char *file_name, const char *function_name)
{
	RTprogram p;
	int err;

	err = rtProgramCreateFromPTXFile(
		material->context, file_name, function_name, &p);
	H_ERR(err, OPXU_ERROR_BAD_PROGRAM);
	err = rtMaterialSetClosestHitProgram(material->material,
		ray_type_index, p);
	H_ERR(err, OPXU_ERROR_BAD_PROGRAM);
error:
	return;
}

/******************************************************************************
 *
 * 			Geometry instance
 *
 *****************************************************************************/

struct opxu_geometry_instance *opxu_context_create_geometry_instance(
	struct opxu_context *context, struct opxu_geometry *geometry,
	unsigned int num_materials)
{
	struct opxu_geometry_instance *gip = calloc(1, sizeof(*gip));
	int err;

	if (!gip) {
		opxu_error_set(OPXU_ERROR_OUT_OF_MEMORY, RT_SUCCESS);
		return NULL;
	}

	assert(context);

	err = rtGeometryInstanceCreate(context->context,
		&gip->geometry_instance);
	H_ERR(err, OPXU_ERROR_OUT_OF_MEMORY);
	err = rtGeometryInstanceSetGeometry(gip->geometry_instance,
		geometry->geometry);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	err = rtGeometryInstanceSetMaterialCount(
		gip->geometry_instance, num_materials);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	gip->next = context->geometry_instances;
	context->geometry_instances = gip;
	gip->context = context->context;
	return gip;
error:
	free(gip);
	return NULL;
}

void opxu_geometry_instance_set_material(
	struct opxu_geometry_instance *geometry_instance,
	unsigned int material_index, struct opxu_material *material)
{
	int err;
	assert(geometry_instance);

	err = rtGeometryInstanceSetMaterial(
		geometry_instance->geometry_instance, material_index,
		material->material);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
error:
	return;
}

/******************************************************************************
 *
 * 			Acceleration
 *
 *****************************************************************************/

struct opxu_acceleration *opxu_context_create_acceleration(
	struct opxu_context *context, const char *builder,
	const char *traverser)
{
	struct opxu_acceleration *ap = calloc(1, sizeof(*ap));
	int err;

	if (!ap) {
		opxu_error_set(OPXU_ERROR_OUT_OF_MEMORY, RT_SUCCESS);
		return NULL;
	}

	assert(context);

	err = rtAccelerationCreate(context->context,
		&ap->acceleration);
	H_ERR(err, OPXU_ERROR_OUT_OF_MEMORY);
	err = rtAccelerationSetBuilder(ap->acceleration,
		builder);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	err = rtAccelerationSetTraverser(ap->acceleration,
		traverser);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	ap->context = context->context;
	ap->next = context->accelerations;
	context->accelerations = ap;
	return ap;
error:
	free(ap);
	return NULL;
}

/******************************************************************************
 *
 * 			Geometry Group
 *
 *****************************************************************************/


struct opxu_geometry_group *opxu_context_create_geometry_group(
	struct opxu_context *context, struct opxu_acceleration *acceleration,
	unsigned int num_children)
{
	struct opxu_geometry_group *ggp = calloc(1, sizeof(*ggp));
	int err;

	err = rtGeometryGroupCreate(context->context, &ggp->geometry_group);
	H_ERR(err, OPXU_ERROR_OUT_OF_MEMORY);
	err = rtGeometryGroupSetChildCount(ggp->geometry_group, num_children);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	err = rtGeometryGroupSetAcceleration(ggp->geometry_group,
		acceleration->acceleration);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
	ggp->context = context->context;
	ggp->next = context->geometry_groups;
	context->geometry_groups = ggp;
	return ggp;
error:
	free(ggp);
	return NULL;
}

void opxu_geometry_group_set_child(struct opxu_geometry_group *geometry_group,
	unsigned int index, struct opxu_geometry_instance *geometry_instance)
{
	int err;
	err = rtGeometryGroupSetChild(geometry_group->geometry_group,
		index, geometry_instance->geometry_instance);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
error:
	return;
}

void opxu_geometry_group_assign_to_variable(
	struct opxu_geometry_group *geometry_group, const char *name)
{
	int err;
	err = rtContextDeclareVariable(geometry_group->context, name,
		&geometry_group->variable);
	H_ERR(err, OPXU_ERROR_OUT_OF_MEMORY);
	err = rtVariableSetObject(geometry_group->variable,
		geometry_group->geometry_group);
	H_ERR(err, OPXU_ERROR_INVALID_ARGUMENTS);
error:
	return;
}

