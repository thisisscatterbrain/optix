#include <optix.h>
#include <optixu/optixu_math_namespace.h>

rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );

RT_PROGRAM void triangle_intersect(int idx /* the primitive index */)
{
	/* note: hardcoded triangle */
	float3 v0 = make_float3(-0.5, -0.5, 0.0);
	float3 v1 = make_float3(0.5, -0.5, 0.0);
	float3 v2 = make_float3(0.5, 0.5, 0.0);
	float u, v, t;
	float3 n;

	if (intersect_triangle(ray, v0, v1, v2, n, t, u, v)) {
		if (rtPotentialIntersection(t)) {
			rtReportIntersection(0);
		}
	}
}

RT_PROGRAM void triangle_bounding_box(int idx, float bb[6])
{
	bb[0] = 0.0;
	bb[1] = 0.0;
	bb[2] = 0.0; // mb some EPS here

	bb[3] = 1.0;
	bb[4] = 1.0;
	bb[5] = 0.0; // mb some EPS here
}
