#include <optix.h>
#include <optixu/optixu_math_namespace.h>
//#include "../common/raytri.cuh"

rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );
rtBuffer<float3, 1> geom_buffer;

RT_PROGRAM void triangle_intersect(int idx /* the primitive index */)
{
	/* note: hardcoded triangle */
	float3 v0 = geom_buffer[idx * 3 + 0];
	float3 v1 = geom_buffer[idx * 3 + 1];
	float3 v2 = geom_buffer[idx * 3 + 2];
	float3 n;
	float u, v, t;

	if (intersect_triangle(ray, v0, v1, v2, n, t ,u, v)) {
		if (rtPotentialIntersection(t)) {
			rtReportIntersection(0);
		}
	}
}

#define BB_EPS 0.01

RT_PROGRAM void triangle_bounding_box(int idx, float bb[6])
{
	bb[0] = 0.0 - BB_EPS;
	bb[1] = 0.0 - BB_EPS;
	bb[2] = 0.0 - BB_EPS; // mb some EPS here

	bb[3] = 1.0 + BB_EPS;
	bb[4] = 1.0 + BB_EPS;
	bb[5] = 0.0 + BB_EPS; // mb some EPS here
}
