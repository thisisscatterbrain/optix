#include <optix.h>
#include "payload.cuh"

rtDeclareVariable(Payload, payload, rtPayload, );

RT_PROGRAM void on_closest_hit()
{
	payload.color = make_float4(1.0, 0.0, 0.0, 1.0);
}
