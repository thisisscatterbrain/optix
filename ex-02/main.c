#include <stdio.h>
#include <optix.h>
#include <assert.h>
#include <stb/stb-ex.h>

#define RT_ASSERT(X) assert(X == RT_SUCCESS);

#define RAYGEN_PTX_FILENAME "raygen.ptx"
#define RAYGEN_FUNCTIONNAME "raygen"
#define TRIANGLE_PTX_FILENAME "triangle.ptx"
#define TRIINTER_FUNCTIONNAME "triangle_intersect"
#define TRIBB_FUNCTIONNAME "triangle_bounding_box"
#define MISS_PTX_FILENAME "miss.ptx"
#define MISS_FUNCTIONNAME "miss"
#define MATERIAL_PTX_FILENAME "material.ptx"
#define MATERIAL_FUNCTIONNAME "on_closest_hit"
#define WIDTH 512
#define HEIGHT 512

static RTcontext _context;
static RTbuffer _buffer;
static RTprogram _raygen_program;
static RTvariable _bufvar;
static RTgeometry _triangle;
static RTprogram _triangle_inters_prog;
static RTprogram _triangle_bb_inters_prog;
static RTprogram _miss_program;
static RTmaterial _material;
static RTprogram _material_program;
static RTgeometrygroup _geometry_group;
static RTvariable _top_node;
static RTacceleration _acceleration;
static RTgeometryinstance _geometry_instance;
static RTbuffer _geometry_buffer;
static float _quad[] = {
		-0.5, -0.5, 0.0,
		+0.5, -0.5, 0.0,
		+0.5, +0.5, 0.0,

		-0.5, -0.5, 0.0,
		+0.5, +0.5, 0.0,
		-0.5, +0.5, 0.0
	};
static RTvariable _geobufvar;

static void dump_buffer(RTbuffer *buffer)
{
	RTsize w, h, i = -1;
	void *data = NULL;
	RT_ASSERT(rtBufferGetSize2D(*buffer, &w, &h));
	RT_ASSERT(rtBufferMap(*buffer, &data));
	stb_ex_writef("test.bmp", w, h, 4, data);
	RT_ASSERT(rtBufferUnmap(*buffer));
}

static void fill_geometry_buffer()
{
	int i = 0;
	RTsize w;
	void *dp;
	float *fp;


	RT_ASSERT(rtBufferGetSize1D(_geometry_buffer, &w));
	RT_ASSERT(rtBufferMap(_geometry_buffer, &dp));
	fp = dp;

	for (i = 0; i < w * 3; i++) {
		fp[i] = _quad[i];
	}

	RT_ASSERT(rtBufferUnmap(_geometry_buffer));
}

static void initialize()
{
	RT_ASSERT(rtContextCreate(&_context));
	RT_ASSERT(rtContextSetRayTypeCount(_context, 1));
	RT_ASSERT(rtContextSetEntryPointCount(_context, 1));

	/* create a buffer for the output frame */
	RT_ASSERT(rtBufferCreate(_context, RT_BUFFER_INPUT_OUTPUT, &_buffer));
	RT_ASSERT(rtBufferSetFormat(_buffer, RT_FORMAT_FLOAT4));
	RT_ASSERT(rtBufferSetSize2D(_buffer, WIDTH, HEIGHT));
	RT_ASSERT(rtContextDeclareVariable(_context, "img_buffer", &_bufvar));
	RT_ASSERT(rtVariableSetObject(_bufvar, _buffer));

	RT_ASSERT(rtProgramCreateFromPTXFile(_context, RAYGEN_PTX_FILENAME,
		RAYGEN_FUNCTIONNAME, &_raygen_program));
	RT_ASSERT(rtContextSetRayGenerationProgram(_context, 0,
		_raygen_program));

	RT_ASSERT(rtProgramCreateFromPTXFile(_context, MISS_PTX_FILENAME,
		MISS_FUNCTIONNAME, &_miss_program));
	RT_ASSERT(rtContextSetMissProgram(_context, 0,
		_miss_program));
}

static void init_geometry()
{
	RT_ASSERT(rtGeometryCreate(_context, &_triangle));
	RT_ASSERT(rtGeometrySetPrimitiveCount(_triangle, 2));
	RT_ASSERT(rtProgramCreateFromPTXFile(_context,
		TRIANGLE_PTX_FILENAME, TRIINTER_FUNCTIONNAME,
		&_triangle_inters_prog));
	RT_ASSERT(rtProgramCreateFromPTXFile(_context,
		TRIANGLE_PTX_FILENAME, TRIBB_FUNCTIONNAME,
		&_triangle_bb_inters_prog));
	RT_ASSERT(rtGeometrySetIntersectionProgram(_triangle,
		_triangle_inters_prog));
	RT_ASSERT(rtGeometrySetBoundingBoxProgram(_triangle,
		_triangle_bb_inters_prog));

	RT_ASSERT(rtBufferCreate(_context, RT_BUFFER_INPUT_OUTPUT,
		&_geometry_buffer));
	RT_ASSERT(rtBufferSetFormat(_geometry_buffer, RT_FORMAT_FLOAT3));
	RT_ASSERT(rtBufferSetSize1D(_geometry_buffer, 6 /* two triangles*/));
	fill_geometry_buffer();
	RT_ASSERT(rtContextDeclareVariable(_context, "geom_buffer",
		&_geobufvar));
	RT_ASSERT(rtVariableSetObject(_geobufvar, _geometry_buffer));
}

static void init_material()
{
	RT_ASSERT(rtMaterialCreate(_context, &_material));
	RT_ASSERT(rtProgramCreateFromPTXFile(_context, MATERIAL_PTX_FILENAME,
		MATERIAL_FUNCTIONNAME, &_material_program));
	RT_ASSERT(rtMaterialSetClosestHitProgram(_material, 0,
		_material_program));
}

static void init_geometry_instance()
{
	RT_ASSERT(rtGeometryInstanceCreate(_context, &_geometry_instance));
	RT_ASSERT(rtGeometryInstanceSetGeometry(_geometry_instance, _triangle));
	RT_ASSERT(rtGeometryInstanceSetMaterialCount(_geometry_instance, 1));
	RT_ASSERT(rtGeometryInstanceSetMaterial(_geometry_instance, 0,
		_material));

	RT_ASSERT(rtAccelerationCreate(_context, &_acceleration));
	RT_ASSERT(rtAccelerationSetBuilder(_acceleration, "NoAccel"));
	RT_ASSERT(rtAccelerationSetTraverser(_acceleration, "NoAccel"));

	RT_ASSERT(rtGeometryGroupCreate(_context, &_geometry_group));
	RT_ASSERT(rtGeometryGroupSetChildCount(_geometry_group, 1));
	RT_ASSERT(rtGeometryGroupSetChild(_geometry_group, 0,
		_geometry_instance));
	RT_ASSERT(rtGeometryGroupSetAcceleration(_geometry_group,
		_acceleration));

	RT_ASSERT(rtContextDeclareVariable(_context, "top_node", &_top_node));
	RT_ASSERT(rtVariableSetObject(_top_node, _geometry_group));
}

static void finalize()
{
	RT_ASSERT(rtContextDestroy(_context));
}

int main(int argc, const char *argv[])
{
	initialize();
	init_geometry();
	init_material();
	init_geometry_instance();

	/* compile the context & run it */
	RT_ASSERT(rtContextCompile(_context));
	RT_ASSERT(rtContextLaunch2D(_context, 0, WIDTH, HEIGHT));

	/* check results */
	dump_buffer(&_buffer);

	finalize();
	return 0;
}

