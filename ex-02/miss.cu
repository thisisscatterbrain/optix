#include <optix.h>
#include "payload.cuh"

rtDeclareVariable(Payload, payload, rtPayload, );

RT_PROGRAM void miss()
{
	payload.color = make_float4(1.0, 1.0, 1.0, 1.0);
}
